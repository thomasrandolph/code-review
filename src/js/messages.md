This app complexity is right on the edge of needing explicitly defined messages 
instead of hard-coded strings everywhere.

To avoid overly abstracting, I'm not creating those individual message files 
but I am going to document all of the messages this app uses so at least it's 
not just scattered everywhere.

### Messages

- `Core:Merge-Requests:Fetch`: An event indicating that we should start making an API request for this MR from GitLab
- `Core:Merge-Requests:Load`: A request to gather a merge request from the local DB
- `Core:Merge-Requests:Changed`: When a merge request and its files have been loaded into the local database
- `Core:Oauth:Fail`: When authenticating with GitLab Oauth fails
- `Core:Oauth:Upgrade`: A chore task that attempts to upgrade a one-time code into a full OAuth access token
- `Core:Logout`: De-authenticates this browser by deleting a known GL OAuth token.
