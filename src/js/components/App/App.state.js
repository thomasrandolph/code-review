import { getState as rootState, getService, applyToComponent } from "../../common/modules/@thomasrandolph/taproot/State.js";

export const AUTHENTICATE_EVENT = "AUTHENTICATE";
export const LOGOUT_EVENT = "LOGOUT";
export const LOAD_EVENT = "LOAD";
export const FILES_EVENT = "FILES";
export const ERROR_EVENT = "ERROR";

export var states = {
	"unauthenticated": {
		"on": {
			[AUTHENTICATE_EVENT]: "empty"
		}
	},
	"empty": {
		"on": {
			[LOGOUT_EVENT]: "unauthenticated",
			[LOAD_EVENT]: "loading",
			[ERROR_EVENT]: "error"
		}
	},
	"loading": {
		"on": {
			[FILES_EVENT]: "files",
			[ERROR_EVENT]: "error"
		}
	},
	"files": {
		"on": {
			[LOAD_EVENT]: "loading",
			[LOGOUT_EVENT]: "unauthenticated"
		}
	},
	"error": {
		"on": {
			[LOGOUT_EVENT]: "unauthenticated",
			[LOAD_EVENT]: "loading"
		}
	}
};

export function getState( { component, id, initial, propertyName = "state", serviceName = "stateService" } = {} ){
	var machine = rootState( {
		"id": id || `app-${component.uuid}`,
		"initial": initial || "unauthenticated",
		states
	} );
	var service = getService( machine );

	applyToComponent( component, service, propertyName, serviceName );
	service.start();

	return service;
}
