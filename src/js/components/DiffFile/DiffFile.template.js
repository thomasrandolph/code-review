import { html, unsafeHTML } from "../../common/modules/@thomasrandolph/taproot/Component.js";

import { syntaxByExtension } from "../../common/file.js";

export function template( { file } = {} ){
	var lex = syntaxByExtension( file.raw.new_path );
	var diff = file.lines.reduce( ( blob, line ) => {
		let text = line;
		let type = "normal";

		if( line.startsWith( "+" ) ){
			type = "addition";
		}
		else if( line.startsWith( "-" ) ){
			type = "deletion";
		}
		else if( line.startsWith( "@@" ) ){
			type = "match";
			text = `// ${line}`;
		}

		if( type != "match" ){
			text = text.replace( /^[+-\s]/, '' );
		}

		return {
			"text": `${blob.text}\n${text}`,
			"fill": [ ...blob.fill, `<div ${type}>&nbsp;</div>` ]
		};
	}, { "text": "", "fill": [ "<div pre-code-ws>&nbsp;</div>" ] } );

	return html`

<div file>
	<div header>
		<strong>${file.raw.new_path}</strong>
	</div>
	<div content>
		<pre><div>${unsafeHTML( diff.fill.join( "" ) )}</div><code class="hljs language-${lex}">${diff.text}</code></pre>
	</div>
</div>

	`;
}
