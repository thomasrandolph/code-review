import { uuid } from "./modules/@thomasrandolph/taproot/Random.js";
import { get } from "./modules/@thomasrandolph/taproot/storage/local.js";

var endpointRE = /^(\/?(.+?)\/(.+?)\/-\/merge_requests\/(\d+)).*$/i;

function getVersionInfo( { endpoint } = {} ){
	var dummyRoot = "https://gitlab.com";
	var endpointUrl = new URL( endpoint, dummyRoot );
	var params = Object.fromEntries( endpointUrl.searchParams.entries() );

	var { "start_sha": startSha, "diff_id": diffId } = params;

	return {
		diffId,
		startSha,
	};
}

export function getDerivedMergeRequestInformation( { endpoint } = {} ){
	var matches = endpointRE.exec( endpoint );
	var mrPath;
	var userOrGroup;
	var project;
	var id;
	var diffId;
	var startSha;

	if (matches) {
		[ , mrPath, userOrGroup, project, id ] = matches;
		( { diffId, startSha } = getVersionInfo( { endpoint } ) );
	}

	return {
		mrPath,
		userOrGroup,
		project,
		id,
		diffId,
		startSha,
	};
}

export function hashPartsToIdentifier( { userOrGroup, project, id } ){
	return uuid( {
		"seeds": [ userOrGroup, project, id ]
	} );
}

export function hashVersionPartsToIdentifier( { userOrGroup, project, id, versionHeadSha } ){
	return uuid( {
		"seeds": [ userOrGroup, project, id, versionHeadSha ]
	} );
}

export function identifier( { metadata } ){
	var { userOrGroup, project, id } = getDerivedMergeRequestInformation( {
		"endpoint": metadata.merge_request_diff.version_path
	} );

	return hashPartsToIdentifier( {
		userOrGroup, project, id
	} );
}

export async function fetchSingle( { org, project, id } ){
	var token = get( "token" );
	var mr = await fetch( "/.netlify/functions/fetchMr", {
		"method": "POST",
		"body": JSON.stringify( {
			org,
			project,
			id,
			token
		} )
	} );

	mr = mr.json();

	return mr ? Promise.resolve( mr ) : Promise.reject();
}
