import { css } from "../../common/modules/@thomasrandolph/taproot/Component.js";

export function styles(){
	return css`

	div{
		display: grid;
		gap: 0.5rem;
	}

	div[authenticated]{
		grid-template-columns: max-content 1fr;
		place-items: left;
	}

	div[unauthenticated]{
		grid-template-columns: 1fr 5fr 1fr;
		place-items: center;
	}

	button[login]{
		grid-column: 2;
	}

	`;
}
