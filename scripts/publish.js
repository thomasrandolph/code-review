import { readFile, writeFile } from "node:fs/promises";

import { build } from "esbuild";
import { globby } from "globby";
import { copy, emptyDir } from "fs-extra";

var pages;
var files;
var components;
var redirects;

await emptyDir( "./public/js" );
console.info( "Emptied public JS directory" );

pages = await globby( [ "./src/js/pages/*.js" ] );

await build( {
	"entryPoints": pages,
	"allowOverwrite": true,
	"bundle": true,
	"splitting": true,
	"chunkNames": "chk/[hash]",
	"minify": true,
	"format": "esm",
	"outdir": "./public/js"
} );

pages.forEach( ( page ) => console.info( `Compiled page-specific app based on ${page}` ) );
