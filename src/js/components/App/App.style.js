import { css } from "../../common/modules/@thomasrandolph/taproot/Component.js";

export function styles(){
	return css`

	@layer component;
	@layer component.base;

	@layer component.base{
		:host{
			font-size: 1.8rem;
			height: 100%;
			line-height: 2.2;
			margin: auto;
			padding: 2rem;
			width: 100%;
		}

		label{
			display: block;
			width: 100%;
		}
		input{
			display: inline-block;
			width: 50vw;
		}

		mcr-diff-file + mcr-diff-file{
			margin-block-start: 4rem;
		}
	}

	`;
}
