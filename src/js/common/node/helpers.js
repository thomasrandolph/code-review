import { request } from "https";

export function getEnvironmentVariable( name ){
	/* eslint-disable-next-line */
	return process.env[ name ];
}

export function fetch( config ){
	return new Promise( ( resolve ) => {
		let resBody = [];
		let req = request(
			config.url,
			config.options,
			( result ) => {
				result.on( "data", ( data ) => {
					resBody.push( data );
				} );
				result.on( "end", () => {
					resBody = JSON.parse( Buffer.concat( resBody ).toString() );

					resolve( resBody );
				} );
			}
		);

		if( config.options.method == "POST" ){
			req.write( config.postBody, "utf8" );
		}
		req.end();
	} )
}
