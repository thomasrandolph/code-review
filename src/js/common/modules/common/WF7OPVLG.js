import {
  clamp,
  stringToNumberHash
} from "./V57FHIZQ.js";

// node_modules/fast-mersenne-twister/mersenne.js
var N = 624;
var N_MINUS_1 = 623;
var M = 397;
var M_MINUS_1 = 396;
var DIFF = N - M;
var MATRIX_A = 2567483615;
var UPPER_MASK = 2147483648;
var LOWER_MASK = 2147483647;
function twist(state) {
  var bits;
  for (let i = 0; i < DIFF; i++) {
    bits = state[i] & UPPER_MASK | state[i + 1] & LOWER_MASK;
    state[i] = state[i + M] ^ bits >>> 1 ^ (bits & 1) * MATRIX_A;
  }
  for (let i = DIFF; i < N_MINUS_1; i++) {
    bits = state[i] & UPPER_MASK | state[i + 1] & LOWER_MASK;
    state[i] = state[i - DIFF] ^ bits >>> 1 ^ (bits & 1) * MATRIX_A;
  }
  bits = state[N_MINUS_1] & UPPER_MASK | state[0] & LOWER_MASK;
  state[N_MINUS_1] = state[M_MINUS_1] ^ bits >>> 1 ^ (bits & 1) * MATRIX_A;
  return state;
}
function initializeWithArray(seedArray) {
  var state = initializeWithNumber(19650218);
  var len = seedArray.length;
  var i = 1;
  var j = 0;
  var k = N > len ? N : len;
  for (; k; k--) {
    let s = state[i - 1] ^ state[i - 1] >>> 30;
    state[i] = (state[i] ^ (((s & 4294901760) >>> 16) * 1664525 << 16) + (s & 65535) * 1664525) + seedArray[j] + j;
    i++;
    j++;
    if (i >= N) {
      state[0] = state[N_MINUS_1];
      i = 1;
    }
    if (j >= len) {
      j = 0;
    }
  }
  for (k = N_MINUS_1; k; k--) {
    let s = state[i - 1] ^ state[i - 1] >>> 30;
    state[i] = (state[i] ^ (((s & 4294901760) >>> 16) * 1566083941 << 16) + (s & 65535) * 1566083941) - i;
    i++;
    if (i >= N) {
      state[0] = state[N_MINUS_1];
      i = 1;
    }
  }
  state[0] = UPPER_MASK;
  return state;
}
function initializeWithNumber(seed) {
  var state = new Array(N);
  state[0] = seed;
  for (let i = 1; i < N; i++) {
    let s = state[i - 1] ^ state[i - 1] >>> 30;
    state[i] = (((s & 4294901760) >>> 16) * 1812433253 << 16) + (s & 65535) * 1812433253 + i;
  }
  return state;
}
function initialize(seed = Date.now()) {
  var state;
  if (Array.isArray(seed)) {
    state = initializeWithArray(seed);
  } else {
    state = initializeWithNumber(seed);
  }
  return twist(state);
}
function MersenneTwister(seed) {
  var state = initialize(seed);
  var next = 0;
  var randomInt32 = () => {
    let x;
    if (next >= N) {
      state = twist(state);
      next = 0;
    }
    x = state[next++];
    x ^= x >>> 11;
    x ^= x << 7 & 2636928640;
    x ^= x << 15 & 4022730752;
    x ^= x >>> 18;
    return x >>> 0;
  };
  var api = {
    // [0,0xffffffff]
    "genrand_int32": () => randomInt32(),
    // [0,0x7fffffff]
    "genrand_int31": () => randomInt32() >>> 1,
    // [0,1]
    "genrand_real1": () => randomInt32() * (1 / 4294967295),
    // [0,1)
    "genrand_real2": () => randomInt32() * (1 / 4294967296),
    // (0,1)
    "genrand_real3": () => (randomInt32() + 0.5) * (1 / 4294967296),
    // [0,1), 53-bit resolution
    "genrand_res53": () => {
      let a = randomInt32() >>> 5;
      let b = randomInt32() >>> 6;
      return (a * 67108864 + b) * (1 / 9007199254740992);
    },
    "randomNumber": () => randomInt32(),
    "random31Bit": () => api.genrand_int31(),
    "randomInclusive": () => api.genrand_real1(),
    "random": () => api.genrand_real2(),
    // returns values just like Math.random
    "randomExclusive": () => api.genrand_real3(),
    "random53Bit": () => api.genrand_res53()
  };
  return api;
}

// node_modules/uuid/dist/esm-browser/rng.js
var getRandomValues;
var rnds8 = new Uint8Array(16);
function rng() {
  if (!getRandomValues) {
    getRandomValues = typeof crypto !== "undefined" && crypto.getRandomValues && crypto.getRandomValues.bind(crypto) || typeof msCrypto !== "undefined" && typeof msCrypto.getRandomValues === "function" && msCrypto.getRandomValues.bind(msCrypto);
    if (!getRandomValues) {
      throw new Error("crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported");
    }
  }
  return getRandomValues(rnds8);
}

// node_modules/uuid/dist/esm-browser/regex.js
var regex_default = /^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$/i;

// node_modules/uuid/dist/esm-browser/validate.js
function validate(uuid2) {
  return typeof uuid2 === "string" && regex_default.test(uuid2);
}
var validate_default = validate;

// node_modules/uuid/dist/esm-browser/stringify.js
var byteToHex = [];
for (i = 0; i < 256; ++i) {
  byteToHex.push((i + 256).toString(16).substr(1));
}
var i;
function stringify(arr) {
  var offset = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : 0;
  var uuid2 = (byteToHex[arr[offset + 0]] + byteToHex[arr[offset + 1]] + byteToHex[arr[offset + 2]] + byteToHex[arr[offset + 3]] + "-" + byteToHex[arr[offset + 4]] + byteToHex[arr[offset + 5]] + "-" + byteToHex[arr[offset + 6]] + byteToHex[arr[offset + 7]] + "-" + byteToHex[arr[offset + 8]] + byteToHex[arr[offset + 9]] + "-" + byteToHex[arr[offset + 10]] + byteToHex[arr[offset + 11]] + byteToHex[arr[offset + 12]] + byteToHex[arr[offset + 13]] + byteToHex[arr[offset + 14]] + byteToHex[arr[offset + 15]]).toLowerCase();
  if (!validate_default(uuid2)) {
    throw TypeError("Stringified UUID is invalid");
  }
  return uuid2;
}
var stringify_default = stringify;

// node_modules/uuid/dist/esm-browser/v4.js
function v4(options, buf, offset) {
  options = options || {};
  var rnds = options.random || (options.rng || rng)();
  rnds[6] = rnds[6] & 15 | 64;
  rnds[8] = rnds[8] & 63 | 128;
  if (buf) {
    offset = offset || 0;
    for (var i = 0; i < 16; ++i) {
      buf[offset + i] = rnds[i];
    }
    return buf;
  }
  return stringify_default(rnds);
}
var v4_default = v4;

// node_modules/@thomasrandolph/taproot/lib/Random.js
function getSeed(seeds) {
  return seeds.reduce((seedling, seed, i) => {
    let thisSeed = 0;
    if (Number.isInteger(seed)) {
      thisSeed = seed;
    } else if (typeof seed == "string") {
      thisSeed = stringToNumberHash(seed);
    }
    return seedling + (seeds.length - i) * thisSeed;
  }, 0);
}
function getPseudoRandomNumberGenerator(...seeds) {
  let seedNumber;
  if (seeds.length) {
    seedNumber = getSeed(seeds);
  } else {
    seedNumber = Math.floor(Math.random() * 10 ** 15);
  }
  return MersenneTwister(seedNumber);
}
function randomValuesForUuid(prng) {
  var randomValues = [];
  for (let i = 0; i <= 3; i += 1) {
    let buffer = new ArrayBuffer(4);
    let view = new DataView(buffer);
    view.setUint32(0, prng.randomNumber());
    randomValues.push(
      view.getUint8(0),
      view.getUint8(1),
      view.getUint8(2),
      view.getUint8(3)
    );
  }
  return randomValues;
}
function uuid({
  seeds = [],
  count = 1
} = {}) {
  var prng = getPseudoRandomNumberGenerator(...seeds);
  var uuids = Array(clamp(count, 1, Number.MAX_SAFE_INTEGER)).fill(0).map(() => v4_default({ "random": randomValuesForUuid(prng) }));
  return count != 1 ? uuids : uuids[0];
}

export {
  uuid
};
