import{a as m,b as f,c as b,d as g}from"./OGHRZLRI.js";import"./2IE65OX3.js";import{a,c as d,d as p,e as s}from"./5N6BXHGL.js";import"./6A2ISCT3.js";var l="AUTHENTICATE",c="LOGOUT",o="LOAD",u="FILES",E="ERROR",L={unauthenticated:{on:{[l]:"empty"}},empty:{on:{[c]:"unauthenticated",[o]:"loading",[E]:"error"}},loading:{on:{[u]:"files",[E]:"error"}},files:{on:{[o]:"loading",[c]:"unauthenticated"}},error:{on:{[c]:"unauthenticated",[o]:"loading"}}};function y({component:r,id:e,initial:i,propertyName:t="state",serviceName:w="stateService"}={}){var C=f({id:e||`app-${r.uuid}`,initial:i||"unauthenticated",states:L}),n=b(C);return g(r,n,t,w),n.start(),n}function h({load:r=()=>{},disabled:e=!1}){return a`

	<label for="mrUrl">MR Url (only works on <code>gitlab.com</code> MRs you have access to)</label>
	<input
		id="mrUrl"
		type="text"
		placeholder="https://gitlab.com/gitlab-org/gitlab/-/merge_requests/101828"
		?disabled="${e}"
	/>
	<button @click="${r}" ?disabled="${e}">Load MR</button>

	`}function v(){return a`<mcr-authenticate></mcr-authenticate>`}function T({files:r,observer:e,load:i}={}){return a`

	<mcr-authenticate></mcr-authenticate>
	<hr />
	${h({load:i})}
	<hr />
	${p(r,t=>t.uuid,t=>a`

		<mcr-diff-file
			.file="${t}"
			.observer="${e}"
		>
		</mcr-diff-file>

		`)}

	`}function x({load:r}){return a`

	<mcr-authenticate></mcr-authenticate>
	<hr />
	${h({load:r})}
	<hr />
	<h2>No files available to display</h2>

	`}function R(){return a`

	<mcr-authenticate></mcr-authenticate>
	<hr />
	${h({load})}
	<hr />
	<h2>No files available to display</h2>
	<p>There was a problem loading the MR.</p>
	`}function N(){return d`

	@layer component;
	@layer component.base;

	@layer component.base{
		:host{
			font-size: 1.8rem;
			height: 100%;
			line-height: 2.2;
			margin: auto;
			padding: 2rem;
			width: 100%;
		}

		label{
			display: block;
			width: 100%;
		}
		input{
			display: inline-block;
			width: 50vw;
		}

		mcr-diff-file + mcr-diff-file{
			margin-block-start: 4rem;
		}
	}

	`}var S=class extends s{static get as(){return`${s.prefix}-app`}static get properties(){return{files:{type:Array},observer:{type:Object,state:!0}}}static get styles(){return[...super.styles,N()]}constructor(){super(),this.files=[],this.observer=new IntersectionObserver(this.seeFile),this.subscriptions=[],y({component:this})}connectedCallback(){super.connectedCallback(),this.subscriptions.push(window.mcr.subscribeWith({"Core:Merge-Requests:Changed":e=>{this.changeMergeRequest(e)}})),m()&&this.stateService.send(l)}disconnectedCallback(){this.subscriptions.forEach(e=>e.unsubscribe()),super.disconnectedCallback()}render(){var e={unauthenticated:()=>v(),empty:()=>x({load:this.load}),loading:()=>loading(),files:()=>T({files:this.files,observer:this.observer,load:this.load}),error:()=>R({load:this.load})};return e[this.state]()}load(e){var i=new URL(e.target.parentNode.querySelector("input").value);this.stateService.send(o),window.mcr.publish({name:"Core:Merge-Requests:Load",url:i.pathname.replace(/^\//,"")})}changeMergeRequest({uuid:e,mr:i,files:t}={}){t.length&&(this.files=t,this.stateService.send(u))}seeFile(e,i){e.forEach(t=>{t.target.highlight()})}};export{S as App};
