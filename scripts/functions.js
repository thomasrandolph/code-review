import { buildSync } from "esbuild";
import { globbySync } from "globby";

var inputs = globbySync( [
	"./src/functions/**/*.js"
] );

buildSync( {
	"entryPoints": inputs,
	"bundle": true,
	"allowOverwrite": true,
	"splitting": false,
	"platform": "node",
	"format": "esm",
	"assetNames": "[dir]/[name]",
	"outdir": "./functions"
} );
