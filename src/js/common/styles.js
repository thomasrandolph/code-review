import { css } from "./modules/@thomasrandolph/taproot/Component.js";

export function base(){
	return css`

	:host{
		display: block;
	}

	h2{
		margin: 0;
		padding: 0;
	}

	`;
}
