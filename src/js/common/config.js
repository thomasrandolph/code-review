import { config } from "../../../config.js";

export function getVariable( { type = "config", name, recurse = false } = {} ){
	var methods = {
		"config": ( n ) => config[ n ]
	};
	var result = methods[ type ]( name );

	return recurse ? getVariable( result ) : result;
}
