import { uuid } from "./modules/@thomasrandolph/taproot/Random.js";

export function syntaxByExtension( filePath ){
	var isJavaScript = filePath.endsWith( ".js" );
	var isHaml = filePath.endsWith( ".haml" );
	var isHTML = filePath.endsWith( ".html" ) || filePath.endsWith( ".vue" );
	var lang = "plaintext";

	if( isJavaScript ){
		lang = "javascript";
	}

	if( isHaml ){
		lang = "haml";
	}

	if( isHTML ){
		lang = "html";
	}

	return lang;
}

export function identifier( { diff, org, project, id, headSha } ){
	return uuid( {
		"seeds": [ org, project, id, headSha, diff.new_path, diff.b_mode ],
	} );
}
