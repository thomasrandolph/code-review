import hljs from "../../common/modules/highlight.js/lib/core.js";
import haml from "../../common/modules/highlight.js/lib/languages/haml.js";
import javascript from "../../common/modules/highlight.js/lib/languages/javascript.js";
import xml from "../../common/modules/highlight.js/lib/languages/xml.js";

import { BaseComponent } from "../Base.js"

import { template } from "./DiffFile.template.js";
import { style } from "./DiffFile.style.js";

hljs.registerLanguage( "haml", haml );
hljs.registerLanguage( "javascript", javascript );
hljs.registerLanguage( "xml", xml );

export class DiffFile extends BaseComponent{
	static get as(){
		return `${BaseComponent.prefix}-diff-file`;
	}

	static get styles(){
		return [
			...super.styles,
			style()
		];
	}

	static get properties(){
		return {
			"file": { "type": Object },
			"observer": { "type": Object }
		};
	}

	constructor(){
		super();

		this.file = null;
		this.observer = null;
	}

	render(){
		return template( { "file": this.file } );
	}

	firstUpdated(){
		this.element = this.shadowRoot.querySelector( "[file]" );

		if( this.observer ){
			this.observer.observe( this );
		}
	}

	highlight(){
		if( this.element ){
			Array
				.from( this.element.querySelectorAll( "pre code.hljs" ) )
				.forEach( ( block ) => {
					hljs.highlightElement( block );
				} );

			this.observer.unobserve( this );
		}
	}
}
