export var schemas = [
	{
		"version": 1,
		"stores": {
			"mergeRequests": "&uuid",
			"mergeRequestVersions": "&uuid",
			"diffFiles": "&uuid"
		}
	}
]
