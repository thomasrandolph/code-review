import { BaseComponent } from "../Base.js"

import { isAuthenticated } from "../../common/authentication.js";

import {
	getState,
	AUTHENTICATE_EVENT,
	LOAD_EVENT,
	FILES_EVENT
} from "./App.state.js";
import { unauthenticated, files, empty, error } from "./App.template.js";
import { styles } from "./App.style.js";

export class App extends BaseComponent{
	static get as(){
		return `${BaseComponent.prefix}-app`;
	}

	static get properties(){
		return {
			"files": { "type": Array },
			"observer": { "type": Object, "state": true }
		};
	}

	static get styles(){
		return [
			...super.styles,
			styles()
		];
	}

	constructor(){
		super();

		this.files = [];
		this.observer = new IntersectionObserver( this.seeFile );

		this.subscriptions = [];

		getState( { "component": this } );
	}
	connectedCallback(){
		super.connectedCallback();

		this.subscriptions.push( window.mcr.subscribeWith( {
			"Core:Merge-Requests:Changed": ( message ) => {
				this.changeMergeRequest( message );
			}
		} ) );

		if( isAuthenticated() ){
			this.stateService.send( AUTHENTICATE_EVENT );
		}
	}
	disconnectedCallback(){
		this.subscriptions.forEach( ( sub ) => sub.unsubscribe() );

		super.disconnectedCallback();
	}

	render(){
		var states = {
			"unauthenticated": () => unauthenticated(),
			"empty": () => empty( {
				"load": this.load
			} ),
			"loading": () => loading(),
			"files": () => files( {
				"files": this.files,
				"observer": this.observer,
				"load": this.load
			} ),
			"error": () => error( {
				"load": this.load
			} )
		};

		return states[ this.state ]();
	}

	load( event ){
		var url = new URL( event.target.parentNode.querySelector( "input" ).value );

		this.stateService.send( LOAD_EVENT );

		window.mcr.publish( {
			"name": "Core:Merge-Requests:Load",
			"url": url.pathname.replace( /^\//, "" )
		} );
	}
	changeMergeRequest( { uuid, mr, files } = {} ){
		if( files.length ){
			this.files = files;
			this.stateService.send( FILES_EVENT );
		}
	}
	seeFile( entries, observer ){
		entries.forEach( ( entry ) => {
			entry.target.highlight();
		} );
	}
}
