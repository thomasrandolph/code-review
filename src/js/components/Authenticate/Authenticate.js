import { BaseComponent } from "../Base.js";

import { isAuthenticated } from "../../common/authentication.js";
import { oauth } from "../../common/gitlab.js";

import { template } from "./Authenticate.template.js";
import { styles } from "./Authenticate.styles.js";
import {
	getState,
	FAILURE_EVENT
} from "./Authenticate.state.js";

export class Authenticate extends BaseComponent{
	static get as(){
		return `${BaseComponent.prefix}-authenticate`;
	}

	static get styles(){
		return [
			...super.styles,
			styles()
		];
	}

	static get properties(){
		return {};
	}

	constructor( ...args ){
		super( ...args );

		this.subscriptions = [];

		getState( { "component": this } );
	}

	connectedCallback( ...args ){
		super.connectedCallback( ...args );

		this.subscriptions.push( window.mcr.subscribeWith( {
			"Core:Oauth:Fail": () => {
				this.stateService.send( FAILURE_EVENT );
			}
		} ) );

		window.mcr.publish( "Core:Oauth:Upgrade" );
	}
	disconnectedCallback( ...args ){
		this.subscriptions.forEach( ( subscription ) => {
			subscription.unsubscribe();
		} );

		super.disconnectedCallback( ...args );
	}

	render(){
		return template( {
			"authenticated": isAuthenticated(),
			"initiate": () => oauth(),
			"logout": () => window.mcr.publish( "Core:Logout" )
		} );
	}
}
