import { uuid } from "./modules/@thomasrandolph/taproot/Random.js";
import { set, get, remove } from "./modules/@thomasrandolph/taproot/storage/local.js";
import { getVariable } from "./config.js";

var domain = window.location.origin;

export var gitlab = {
	"clientId": getVariable( { "type": "config", "name": "appId" } ),
	"redirectUri": `${domain}/?provider=gitlab`,
	"scope": "api",
	"params": ( c, r, sc, st ) => {
		let params = new URLSearchParams( {
			"client_id": c,
			"redirect_uri": r,
			"scope": sc,
			"response_type": "code",
			"state": st
		} );

		return params.toString();
	},
	"url": ( params ) => `https://gitlab.com/oauth/authorize?${params}`
};

export function oauth(){
	var state = uuid();

	set( "state", state );

	window.location = gitlab.url( gitlab.params( gitlab.clientId, gitlab.redirectUri, gitlab.scope, state ) );
}

export function localLogout(){
	remove( "token" );
	remove( "state" );

	window.location = window.location.origin;
}

export async function upgradeCode( params ){
	var state = params.get( "state" );
	var upgrade;
	var json;

	if( state == get( "state" ) ){
		upgrade = await fetch( "/.netlify/functions/upgradeCode", {
			"method": "POST",
			"body": JSON.stringify( {
				"redirect": gitlab.redirectUri,
				"code": params.get( "code" )
			} )
		} );

		json = upgrade.json();
	}

	return json ? Promise.resolve( json ) : Promise.reject();
}
