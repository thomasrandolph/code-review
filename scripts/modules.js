import { emptyDir } from "fs-extra";
import { build } from "esbuild";

const DESTINATION = "./src/js/common/modules/";
var dependencies = [
	"@thomasrandolph/taproot",
	"@thomasrandolph/taproot/Component",
	"@thomasrandolph/taproot/Random",
	"@thomasrandolph/taproot/State",
	"@thomasrandolph/taproot/storage/local",
	"dexie",
	"highlight.js/lib/core",
	"highlight.js/lib/languages/haml",
	"highlight.js/lib/languages/javascript",
	"highlight.js/lib/languages/xml",
	"lit-html",
	"lit-html/directives/unsafe-html"
];

async function run(){
	var files = dependencies.map( ( pkg ) => {
		return `./node_modules/${pkg}`;
	} );

	await emptyDir( "./src/js/common/modules" );
	console.info( "Emptied module directory" );

	await build( {
		"entryPoints": files,
		"allowOverwrite": true,
		"bundle": true,
		"splitting": true,
		"chunkNames": "common/[hash]",
		"minify": false,
		"format": "esm",
		"outdir": DESTINATION
	} );
}

run();
