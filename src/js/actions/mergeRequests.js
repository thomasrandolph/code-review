import { identifier } from "../common/file.js";
import {
	fetchSingle,
	getDerivedMergeRequestInformation,
	hashPartsToIdentifier,
	hashVersionPartsToIdentifier
} from "../common/mr.js";

export function registerMergeRequestActions( { db, subscribeWith, publish } ){
	subscribeWith( {
		"Core:Merge-Requests:Load": async ( message ) => {
			var { url } = message;
			var { userOrGroup, project, id } = getDerivedMergeRequestInformation( { "endpoint": url } );
			var uuid = hashPartsToIdentifier( { userOrGroup, project, id } );

			publish( {
				"name": "Core:Merge-Requests:Fetch",
				"org": userOrGroup,
				project,
				id
			} );

			db
				.transaction( 'r', db.mergeRequests, db.mergeRequestVersions, db.diffFiles, async () => {
					let mr = await db.mergeRequests.get( uuid );
					let version;
					let files;

					if( mr ){
						version = await db
							.mergeRequestVersions
							.get( mr.latestVersion );
					}

					if( version ){
						files = await db
							.diffFiles
							.bulkGet( version.files );
					}

					return { "mr": version, files };
				} )
				.then( ( { mr, files } ) => {
					publish( {
						"name": "Core:Merge-Requests:Changed",
						"uuid": mr.uuid,
						mr,
						files
					} );
				} );
		},
		"Core:Merge-Requests:Fetch": async ( message ) => {
			var { org, project, id } = message;
			var uuid = hashPartsToIdentifier( { "userOrGroup": org, project, id } );
			var { mr, diffs, versions } = await fetchSingle( { org, project, id } );
			var storeableDiffs = diffs.map( ( diff ) => {
				var diffUuid = identifier( {
					"headSha": mr.diff_refs.head_sha,
					diff,
					org,
					project,
					id
				} );

				return {
					"uuid": diffUuid,
					"lines": diff.diff.split( "\n" ),
					"raw": diff
				};
			} );
			var versionUuid = hashVersionPartsToIdentifier( {
				"userOrGroup": org,
				"versionHeadSha": mr.diff_refs.head_sha,
				project,
				id
			} );
			var storeableMr = {
				uuid,
				"latestVersion": versionUuid,
				"versions": versions.map( ( version ) => hashVersionPartsToIdentifier( {
					"userOrGroup": org,
					"versionHeadSha": version.head_commit_sha,
					project,
					id
				} ) )
			};
			var storeableVersion = {
				"uuid": versionUuid,
				"files": storeableDiffs.map( ( diff ) => diff.uuid ),
				"raw": mr
			};

			db
				.transaction( 'rw', db.mergeRequests, db.mergeRequestVersions, db.diffFiles, async () => {
					await db.mergeRequests.put( storeableMr );
					await db.mergeRequestVersions.put( storeableVersion );
					await db.diffFiles.bulkPut( storeableDiffs );
				} )
				.then( () => {
					publish( {
						"name": "Core:Merge-Requests:Changed",
						"uuid": storeableVersion.uuid,
						"mr": storeableVersion,
						"files": storeableDiffs
					} );
				} );
		}
	} );
}
