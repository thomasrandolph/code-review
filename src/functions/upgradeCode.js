import { getVariable } from "../js/common/node/config.js";
import { fetch } from "../js/common/node/helpers.js";

export async function handler( event ){
	var body = JSON.parse( event.body );
	var postBody = {
		"client_id": getVariable( { "type": "config", "name": "appId" } ),
		"client_secret": getVariable( { "type": "config", "name": "appSecretVar", "recurse": true } ),
		"code": body.code,
		"grant_type": "authorization_code",
		"redirect_uri": body.redirect
	};
	var config = {
		"url": "https://gitlab.com/oauth/token",
		"options": {
			"method": "POST",
			"body": JSON.stringify( postBody ),
			"headers": {
				"Accept": "application/json",
				"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
			}
		},
		"postBody": ( new URLSearchParams( postBody ) ).toString()
	};
	var status = 400;
	var responseBody = JSON.stringify( { "error": "Unknown provider" } );
	var auth = await fetch( config );

	status = 200;
	responseBody = JSON.stringify( auth );

	return {
		"statusCode": status,
		"body": responseBody
	};
}
