import { get, set, remove } from "../common/modules/@thomasrandolph/taproot/storage/local.js";

import { localLogout, upgradeCode } from "../common/gitlab.js";

export function registerAuthenticationActions( { subscribeWith, publish } ){
	subscribeWith( {
		"Core:Oauth:Upgrade": async ( message ) => {
			let params = new URLSearchParams( window.location.search );
			let state = get( "state" );
			let access;

			if( state ){
				try{
					access = await upgradeCode( params );

					set( "token", access.access_token );
					remove( "state" );

					window.location = window.location.origin;
				}
				catch( e ){
					publish( {
						"name": "Core:Oauth:Fail",
						"code": params.code,
						"state": get( "state" )
					} );
				}
			}
		},
		"Core:Logout": ( message ) => {
			localLogout();

			window.location.reload();
		}
	} );
}
