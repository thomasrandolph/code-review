export var config = {
	"namespace": "mcr",
	"appId": "5477db67ee287de4d264cbbca50af295e2aa32728cddd99fbf512b71ab5b0a6b",
	"appSecretVar": {
		"type": "env",
		"name": "GITLAB_MCR_APP_SECRET"
	}
};
