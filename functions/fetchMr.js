// src/js/common/node/helpers.js
import { request } from "https";
function fetch(config) {
  return new Promise((resolve) => {
    let resBody = [];
    let req = request(
      config.url,
      config.options,
      (result) => {
        result.on("data", (data) => {
          resBody.push(data);
        });
        result.on("end", () => {
          resBody = JSON.parse(Buffer.concat(resBody).toString());
          resolve(resBody);
        });
      }
    );
    if (config.options.method == "POST") {
      req.write(config.postBody, "utf8");
    }
    req.end();
  });
}

// src/functions/fetchMr.js
function getJsonOptions({ token }) {
  return {
    "method": "GET",
    "headers": {
      "Authorization": `Bearer ${token}`,
      "Accept": "application/json",
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
    }
  };
}
function getMr({ token, org, project, id }) {
  var config = {
    "url": `https://gitlab.com/api/v4/projects/${org}%2F${project}/merge_requests/${id}`,
    "options": getJsonOptions({ token })
  };
  return fetch(config);
}
function getVersions({ token, org, project, id }) {
  var config = {
    "url": `https://gitlab.com/api/v4/projects/${org}%2F${project}/merge_requests/${id}/versions`,
    "options": getJsonOptions({ token })
  };
  return fetch(config);
}
function getDiffs({ token, org, project, id }) {
  var config = {
    "url": `https://gitlab.com/api/v4/projects/${org}%2F${project}/merge_requests/${id}/diffs`,
    "options": getJsonOptions({ token })
  };
  return fetch(config);
}
async function handler(event) {
  var body = JSON.parse(event.body);
  var { token, org, project, id } = body;
  var status = 400;
  var responseBody = JSON.stringify({ "error": "Failed to fetch MR" });
  var [mr, diffs, versions] = await Promise.all([
    getMr({ token, org, project, id }),
    getDiffs({ token, org, project, id }),
    getVersions({ token, org, project, id })
  ]);
  status = 200;
  responseBody = JSON.stringify({ mr, diffs, versions });
  return {
    "statusCode": status,
    "body": responseBody
  };
}
export {
  handler
};
