import {
  e,
  e2,
  i,
  o,
  t
} from "../../common/6ZQF2T23.js";
import {
  L,
  Z,
  b,
  x,
  y
} from "../../common/MVZFSFXA.js";
import {
  getNamespace
} from "../../common/I4BDYMTP.js";
import {
  uuid
} from "../../common/WF7OPVLG.js";
import "../../common/V57FHIZQ.js";
import "../../common/MWZFWPW5.js";

// node_modules/@lit/reactive-element/css-tag.js
var t2 = window;
var e3 = t2.ShadowRoot && (void 0 === t2.ShadyCSS || t2.ShadyCSS.nativeShadow) && "adoptedStyleSheets" in Document.prototype && "replace" in CSSStyleSheet.prototype;
var s = Symbol();
var n = /* @__PURE__ */ new WeakMap();
var o2 = class {
  constructor(t6, e7, n9) {
    if (this._$cssResult$ = true, n9 !== s)
      throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");
    this.cssText = t6, this.t = e7;
  }
  get styleSheet() {
    let t6 = this.o;
    const s7 = this.t;
    if (e3 && void 0 === t6) {
      const e7 = void 0 !== s7 && 1 === s7.length;
      e7 && (t6 = n.get(s7)), void 0 === t6 && ((this.o = t6 = new CSSStyleSheet()).replaceSync(this.cssText), e7 && n.set(s7, t6));
    }
    return t6;
  }
  toString() {
    return this.cssText;
  }
};
var r = (t6) => new o2("string" == typeof t6 ? t6 : t6 + "", void 0, s);
var i2 = (t6, ...e7) => {
  const n9 = 1 === t6.length ? t6[0] : e7.reduce((e8, s7, n10) => e8 + ((t7) => {
    if (true === t7._$cssResult$)
      return t7.cssText;
    if ("number" == typeof t7)
      return t7;
    throw Error("Value passed to 'css' function must be a 'css' function result: " + t7 + ". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.");
  })(s7) + t6[n10 + 1], t6[0]);
  return new o2(n9, t6, s);
};
var S = (s7, n9) => {
  e3 ? s7.adoptedStyleSheets = n9.map((t6) => t6 instanceof CSSStyleSheet ? t6 : t6.styleSheet) : n9.forEach((e7) => {
    const n10 = document.createElement("style"), o14 = t2.litNonce;
    void 0 !== o14 && n10.setAttribute("nonce", o14), n10.textContent = e7.cssText, s7.appendChild(n10);
  });
};
var c = e3 ? (t6) => t6 : (t6) => t6 instanceof CSSStyleSheet ? ((t7) => {
  let e7 = "";
  for (const s7 of t7.cssRules)
    e7 += s7.cssText;
  return r(e7);
})(t6) : t6;

// node_modules/@lit/reactive-element/reactive-element.js
var s2;
var e4 = window;
var r2 = e4.trustedTypes;
var h = r2 ? r2.emptyScript : "";
var o3 = e4.reactiveElementPolyfillSupport;
var n2 = { toAttribute(t6, i7) {
  switch (i7) {
    case Boolean:
      t6 = t6 ? h : null;
      break;
    case Object:
    case Array:
      t6 = null == t6 ? t6 : JSON.stringify(t6);
  }
  return t6;
}, fromAttribute(t6, i7) {
  let s7 = t6;
  switch (i7) {
    case Boolean:
      s7 = null !== t6;
      break;
    case Number:
      s7 = null === t6 ? null : Number(t6);
      break;
    case Object:
    case Array:
      try {
        s7 = JSON.parse(t6);
      } catch (t7) {
        s7 = null;
      }
  }
  return s7;
} };
var a = (t6, i7) => i7 !== t6 && (i7 == i7 || t6 == t6);
var l = { attribute: true, type: String, converter: n2, reflect: false, hasChanged: a };
var d = class extends HTMLElement {
  constructor() {
    super(), this._$Ei = /* @__PURE__ */ new Map(), this.isUpdatePending = false, this.hasUpdated = false, this._$El = null, this.u();
  }
  static addInitializer(t6) {
    var i7;
    this.finalize(), (null !== (i7 = this.h) && void 0 !== i7 ? i7 : this.h = []).push(t6);
  }
  static get observedAttributes() {
    this.finalize();
    const t6 = [];
    return this.elementProperties.forEach((i7, s7) => {
      const e7 = this._$Ep(s7, i7);
      void 0 !== e7 && (this._$Ev.set(e7, s7), t6.push(e7));
    }), t6;
  }
  static createProperty(t6, i7 = l) {
    if (i7.state && (i7.attribute = false), this.finalize(), this.elementProperties.set(t6, i7), !i7.noAccessor && !this.prototype.hasOwnProperty(t6)) {
      const s7 = "symbol" == typeof t6 ? Symbol() : "__" + t6, e7 = this.getPropertyDescriptor(t6, s7, i7);
      void 0 !== e7 && Object.defineProperty(this.prototype, t6, e7);
    }
  }
  static getPropertyDescriptor(t6, i7, s7) {
    return { get() {
      return this[i7];
    }, set(e7) {
      const r5 = this[t6];
      this[i7] = e7, this.requestUpdate(t6, r5, s7);
    }, configurable: true, enumerable: true };
  }
  static getPropertyOptions(t6) {
    return this.elementProperties.get(t6) || l;
  }
  static finalize() {
    if (this.hasOwnProperty("finalized"))
      return false;
    this.finalized = true;
    const t6 = Object.getPrototypeOf(this);
    if (t6.finalize(), void 0 !== t6.h && (this.h = [...t6.h]), this.elementProperties = new Map(t6.elementProperties), this._$Ev = /* @__PURE__ */ new Map(), this.hasOwnProperty("properties")) {
      const t7 = this.properties, i7 = [...Object.getOwnPropertyNames(t7), ...Object.getOwnPropertySymbols(t7)];
      for (const s7 of i7)
        this.createProperty(s7, t7[s7]);
    }
    return this.elementStyles = this.finalizeStyles(this.styles), true;
  }
  static finalizeStyles(i7) {
    const s7 = [];
    if (Array.isArray(i7)) {
      const e7 = new Set(i7.flat(1 / 0).reverse());
      for (const i8 of e7)
        s7.unshift(c(i8));
    } else
      void 0 !== i7 && s7.push(c(i7));
    return s7;
  }
  static _$Ep(t6, i7) {
    const s7 = i7.attribute;
    return false === s7 ? void 0 : "string" == typeof s7 ? s7 : "string" == typeof t6 ? t6.toLowerCase() : void 0;
  }
  u() {
    var t6;
    this._$E_ = new Promise((t7) => this.enableUpdating = t7), this._$AL = /* @__PURE__ */ new Map(), this._$Eg(), this.requestUpdate(), null === (t6 = this.constructor.h) || void 0 === t6 || t6.forEach((t7) => t7(this));
  }
  addController(t6) {
    var i7, s7;
    (null !== (i7 = this._$ES) && void 0 !== i7 ? i7 : this._$ES = []).push(t6), void 0 !== this.renderRoot && this.isConnected && (null === (s7 = t6.hostConnected) || void 0 === s7 || s7.call(t6));
  }
  removeController(t6) {
    var i7;
    null === (i7 = this._$ES) || void 0 === i7 || i7.splice(this._$ES.indexOf(t6) >>> 0, 1);
  }
  _$Eg() {
    this.constructor.elementProperties.forEach((t6, i7) => {
      this.hasOwnProperty(i7) && (this._$Ei.set(i7, this[i7]), delete this[i7]);
    });
  }
  createRenderRoot() {
    var t6;
    const s7 = null !== (t6 = this.shadowRoot) && void 0 !== t6 ? t6 : this.attachShadow(this.constructor.shadowRootOptions);
    return S(s7, this.constructor.elementStyles), s7;
  }
  connectedCallback() {
    var t6;
    void 0 === this.renderRoot && (this.renderRoot = this.createRenderRoot()), this.enableUpdating(true), null === (t6 = this._$ES) || void 0 === t6 || t6.forEach((t7) => {
      var i7;
      return null === (i7 = t7.hostConnected) || void 0 === i7 ? void 0 : i7.call(t7);
    });
  }
  enableUpdating(t6) {
  }
  disconnectedCallback() {
    var t6;
    null === (t6 = this._$ES) || void 0 === t6 || t6.forEach((t7) => {
      var i7;
      return null === (i7 = t7.hostDisconnected) || void 0 === i7 ? void 0 : i7.call(t7);
    });
  }
  attributeChangedCallback(t6, i7, s7) {
    this._$AK(t6, s7);
  }
  _$EO(t6, i7, s7 = l) {
    var e7;
    const r5 = this.constructor._$Ep(t6, s7);
    if (void 0 !== r5 && true === s7.reflect) {
      const h6 = (void 0 !== (null === (e7 = s7.converter) || void 0 === e7 ? void 0 : e7.toAttribute) ? s7.converter : n2).toAttribute(i7, s7.type);
      this._$El = t6, null == h6 ? this.removeAttribute(r5) : this.setAttribute(r5, h6), this._$El = null;
    }
  }
  _$AK(t6, i7) {
    var s7;
    const e7 = this.constructor, r5 = e7._$Ev.get(t6);
    if (void 0 !== r5 && this._$El !== r5) {
      const t7 = e7.getPropertyOptions(r5), h6 = "function" == typeof t7.converter ? { fromAttribute: t7.converter } : void 0 !== (null === (s7 = t7.converter) || void 0 === s7 ? void 0 : s7.fromAttribute) ? t7.converter : n2;
      this._$El = r5, this[r5] = h6.fromAttribute(i7, t7.type), this._$El = null;
    }
  }
  requestUpdate(t6, i7, s7) {
    let e7 = true;
    void 0 !== t6 && (((s7 = s7 || this.constructor.getPropertyOptions(t6)).hasChanged || a)(this[t6], i7) ? (this._$AL.has(t6) || this._$AL.set(t6, i7), true === s7.reflect && this._$El !== t6 && (void 0 === this._$EC && (this._$EC = /* @__PURE__ */ new Map()), this._$EC.set(t6, s7))) : e7 = false), !this.isUpdatePending && e7 && (this._$E_ = this._$Ej());
  }
  async _$Ej() {
    this.isUpdatePending = true;
    try {
      await this._$E_;
    } catch (t7) {
      Promise.reject(t7);
    }
    const t6 = this.scheduleUpdate();
    return null != t6 && await t6, !this.isUpdatePending;
  }
  scheduleUpdate() {
    return this.performUpdate();
  }
  performUpdate() {
    var t6;
    if (!this.isUpdatePending)
      return;
    this.hasUpdated, this._$Ei && (this._$Ei.forEach((t7, i8) => this[i8] = t7), this._$Ei = void 0);
    let i7 = false;
    const s7 = this._$AL;
    try {
      i7 = this.shouldUpdate(s7), i7 ? (this.willUpdate(s7), null === (t6 = this._$ES) || void 0 === t6 || t6.forEach((t7) => {
        var i8;
        return null === (i8 = t7.hostUpdate) || void 0 === i8 ? void 0 : i8.call(t7);
      }), this.update(s7)) : this._$Ek();
    } catch (t7) {
      throw i7 = false, this._$Ek(), t7;
    }
    i7 && this._$AE(s7);
  }
  willUpdate(t6) {
  }
  _$AE(t6) {
    var i7;
    null === (i7 = this._$ES) || void 0 === i7 || i7.forEach((t7) => {
      var i8;
      return null === (i8 = t7.hostUpdated) || void 0 === i8 ? void 0 : i8.call(t7);
    }), this.hasUpdated || (this.hasUpdated = true, this.firstUpdated(t6)), this.updated(t6);
  }
  _$Ek() {
    this._$AL = /* @__PURE__ */ new Map(), this.isUpdatePending = false;
  }
  get updateComplete() {
    return this.getUpdateComplete();
  }
  getUpdateComplete() {
    return this._$E_;
  }
  shouldUpdate(t6) {
    return true;
  }
  update(t6) {
    void 0 !== this._$EC && (this._$EC.forEach((t7, i7) => this._$EO(i7, this[i7], t7)), this._$EC = void 0), this._$Ek();
  }
  updated(t6) {
  }
  firstUpdated(t6) {
  }
};
d.finalized = true, d.elementProperties = /* @__PURE__ */ new Map(), d.elementStyles = [], d.shadowRootOptions = { mode: "open" }, null == o3 || o3({ ReactiveElement: d }), (null !== (s2 = e4.reactiveElementVersions) && void 0 !== s2 ? s2 : e4.reactiveElementVersions = []).push("1.6.1");

// node_modules/lit-element/lit-element.js
var l2;
var o4;
var s3 = class extends d {
  constructor() {
    super(...arguments), this.renderOptions = { host: this }, this._$Do = void 0;
  }
  createRenderRoot() {
    var t6, e7;
    const i7 = super.createRenderRoot();
    return null !== (t6 = (e7 = this.renderOptions).renderBefore) && void 0 !== t6 || (e7.renderBefore = i7.firstChild), i7;
  }
  update(t6) {
    const i7 = this.render();
    this.hasUpdated || (this.renderOptions.isConnected = this.isConnected), super.update(t6), this._$Do = Z(i7, this.renderRoot, this.renderOptions);
  }
  connectedCallback() {
    var t6;
    super.connectedCallback(), null === (t6 = this._$Do) || void 0 === t6 || t6.setConnected(true);
  }
  disconnectedCallback() {
    var t6;
    super.disconnectedCallback(), null === (t6 = this._$Do) || void 0 === t6 || t6.setConnected(false);
  }
  render() {
    return x;
  }
};
s3.finalized = true, s3._$litElement$ = true, null === (l2 = globalThis.litElementHydrateSupport) || void 0 === l2 || l2.call(globalThis, { LitElement: s3 });
var n3 = globalThis.litElementPolyfillSupport;
null == n3 || n3({ LitElement: s3 });
(null !== (o4 = globalThis.litElementVersions) && void 0 !== o4 ? o4 : globalThis.litElementVersions = []).push("3.2.2");

// node_modules/lit-html/directive-helpers.js
var { I: l3 } = L;
var t3 = (o14) => null === o14 || "object" != typeof o14 && "function" != typeof o14;
var n4 = (o14, l7) => void 0 === l7 ? void 0 !== (null == o14 ? void 0 : o14._$litType$) : (null == o14 ? void 0 : o14._$litType$) === l7;
var e5 = (o14) => void 0 === o14.strings;
var c2 = () => document.createComment("");
var r3 = (o14, t6, i7) => {
  var n9;
  const d3 = o14._$AA.parentNode, v = void 0 === t6 ? o14._$AB : t6._$AA;
  if (void 0 === i7) {
    const t7 = d3.insertBefore(c2(), v), n10 = d3.insertBefore(c2(), v);
    i7 = new l3(t7, n10, o14, o14.options);
  } else {
    const l7 = i7._$AB.nextSibling, t7 = i7._$AM, e7 = t7 !== o14;
    if (e7) {
      let l8;
      null === (n9 = i7._$AQ) || void 0 === n9 || n9.call(i7, o14), i7._$AM = o14, void 0 !== i7._$AP && (l8 = o14._$AU) !== t7._$AU && i7._$AP(l8);
    }
    if (l7 !== v || e7) {
      let o15 = i7._$AA;
      for (; o15 !== l7; ) {
        const l8 = o15.nextSibling;
        d3.insertBefore(o15, v), o15 = l8;
      }
    }
  }
  return i7;
};
var u = (o14, l7, t6 = o14) => (o14._$AI(l7, t6), o14);
var f = {};
var s4 = (o14, l7 = f) => o14._$AH = l7;
var m = (o14) => o14._$AH;
var p = (o14) => {
  var l7;
  null === (l7 = o14._$AP) || void 0 === l7 || l7.call(o14, false, true);
  let t6 = o14._$AA;
  const i7 = o14._$AB.nextSibling;
  for (; t6 !== i7; ) {
    const o15 = t6.nextSibling;
    t6.remove(), t6 = o15;
  }
};
var a2 = (o14) => {
  o14._$AR();
};

// node_modules/lit-html/async-directive.js
var s5 = (i7, t6) => {
  var e7, o14;
  const r5 = i7._$AN;
  if (void 0 === r5)
    return false;
  for (const i8 of r5)
    null === (o14 = (e7 = i8)._$AO) || void 0 === o14 || o14.call(e7, t6, false), s5(i8, t6);
  return true;
};
var o5 = (i7) => {
  let t6, e7;
  do {
    if (void 0 === (t6 = i7._$AM))
      break;
    e7 = t6._$AN, e7.delete(i7), i7 = t6;
  } while (0 === (null == e7 ? void 0 : e7.size));
};
var r4 = (i7) => {
  for (let t6; t6 = i7._$AM; i7 = t6) {
    let e7 = t6._$AN;
    if (void 0 === e7)
      t6._$AN = e7 = /* @__PURE__ */ new Set();
    else if (e7.has(i7))
      break;
    e7.add(i7), l4(t6);
  }
};
function n5(i7) {
  void 0 !== this._$AN ? (o5(this), this._$AM = i7, r4(this)) : this._$AM = i7;
}
function h2(i7, t6 = false, e7 = 0) {
  const r5 = this._$AH, n9 = this._$AN;
  if (void 0 !== n9 && 0 !== n9.size)
    if (t6)
      if (Array.isArray(r5))
        for (let i8 = e7; i8 < r5.length; i8++)
          s5(r5[i8], false), o5(r5[i8]);
      else
        null != r5 && (s5(r5, false), o5(r5));
    else
      s5(this, i7);
}
var l4 = (i7) => {
  var t6, s7, o14, r5;
  i7.type == t.CHILD && (null !== (t6 = (o14 = i7)._$AP) && void 0 !== t6 || (o14._$AP = h2), null !== (s7 = (r5 = i7)._$AQ) && void 0 !== s7 || (r5._$AQ = n5));
};
var c3 = class extends i {
  constructor() {
    super(...arguments), this._$AN = void 0;
  }
  _$AT(i7, t6, e7) {
    super._$AT(i7, t6, e7), r4(this), this.isConnected = i7._$AU;
  }
  _$AO(i7, t6 = true) {
    var e7, r5;
    i7 !== this.isConnected && (this.isConnected = i7, i7 ? null === (e7 = this.reconnected) || void 0 === e7 || e7.call(this) : null === (r5 = this.disconnected) || void 0 === r5 || r5.call(this)), t6 && (s5(this, i7), o5(this));
  }
  setValue(t6) {
    if (e5(this._$Ct))
      this._$Ct._$AI(t6, this);
    else {
      const i7 = [...this._$Ct._$AH];
      i7[this._$Ci] = t6, this._$Ct._$AI(i7, this, 0);
    }
  }
  disconnected() {
  }
  reconnected() {
  }
};

// node_modules/lit-html/directives/private-async-helpers.js
var t4 = async (t6, s7) => {
  for await (const i7 of t6)
    if (false === await s7(i7))
      return;
};
var s6 = class {
  constructor(t6) {
    this.Y = t6;
  }
  disconnect() {
    this.Y = void 0;
  }
  reconnect(t6) {
    this.Y = t6;
  }
  deref() {
    return this.Y;
  }
};
var i3 = class {
  constructor() {
    this.Z = void 0, this.q = void 0;
  }
  get() {
    return this.Z;
  }
  pause() {
    var t6;
    null !== (t6 = this.Z) && void 0 !== t6 || (this.Z = new Promise((t7) => this.q = t7));
  }
  resume() {
    var t6;
    null === (t6 = this.q) || void 0 === t6 || t6.call(this), this.Z = this.q = void 0;
  }
};

// node_modules/lit-html/directives/async-replace.js
var o6 = class extends c3 {
  constructor() {
    super(...arguments), this._$CK = new s6(this), this._$CX = new i3();
  }
  render(i7, s7) {
    return x;
  }
  update(i7, [s7, r5]) {
    if (this.isConnected || this.disconnected(), s7 === this._$CJ)
      return;
    this._$CJ = s7;
    let n9 = 0;
    const { _$CK: o14, _$CX: h6 } = this;
    return t4(s7, async (t6) => {
      for (; h6.get(); )
        await h6.get();
      const i8 = o14.deref();
      if (void 0 !== i8) {
        if (i8._$CJ !== s7)
          return false;
        void 0 !== r5 && (t6 = r5(t6, n9)), i8.commitValue(t6, n9), n9++;
      }
      return true;
    }), x;
  }
  commitValue(t6, i7) {
    this.setValue(t6);
  }
  disconnected() {
    this._$CK.disconnect(), this._$CX.pause();
  }
  reconnected() {
    this._$CK.reconnect(this), this._$CX.resume();
  }
};
var h3 = e(o6);

// node_modules/lit-html/directives/async-append.js
var c4 = e(class extends o6 {
  constructor(r5) {
    if (super(r5), r5.type !== t.CHILD)
      throw Error("asyncAppend can only be used in child expressions");
  }
  update(r5, e7) {
    return this._$Ctt = r5, super.update(r5, e7);
  }
  commitValue(r5, e7) {
    0 === e7 && a2(this._$Ctt);
    const s7 = r3(this._$Ctt);
    u(s7, r5);
  }
});

// node_modules/lit-html/directives/cache.js
var d2 = e(class extends i {
  constructor(t6) {
    super(t6), this.et = /* @__PURE__ */ new WeakMap();
  }
  render(t6) {
    return [t6];
  }
  update(s7, [e7]) {
    if (n4(this.it) && (!n4(e7) || this.it.strings !== e7.strings)) {
      const e8 = m(s7).pop();
      let o14 = this.et.get(this.it.strings);
      if (void 0 === o14) {
        const s8 = document.createDocumentFragment();
        o14 = Z(b, s8), o14.setConnected(false), this.et.set(this.it.strings, o14);
      }
      s4(o14, [e8]), r3(o14, void 0, e8);
    }
    if (n4(e7)) {
      if (!n4(this.it) || this.it.strings !== e7.strings) {
        const t6 = this.et.get(e7.strings);
        if (void 0 !== t6) {
          const i7 = m(t6).pop();
          a2(s7), r3(s7, void 0, i7), s4(s7, [i7]);
        }
      }
      this.it = e7;
    } else
      this.it = void 0;
    return this.render(e7);
  }
});

// node_modules/lit-html/directives/choose.js
var o7 = (o14, r5, n9) => {
  for (const n10 of r5)
    if (n10[0] === o14)
      return (0, n10[1])();
  return null == n9 ? void 0 : n9();
};

// node_modules/lit-html/directives/class-map.js
var o8 = e(class extends i {
  constructor(t6) {
    var i7;
    if (super(t6), t6.type !== t.ATTRIBUTE || "class" !== t6.name || (null === (i7 = t6.strings) || void 0 === i7 ? void 0 : i7.length) > 2)
      throw Error("`classMap()` can only be used in the `class` attribute and must be the only part in the attribute.");
  }
  render(t6) {
    return " " + Object.keys(t6).filter((i7) => t6[i7]).join(" ") + " ";
  }
  update(i7, [s7]) {
    var r5, o14;
    if (void 0 === this.nt) {
      this.nt = /* @__PURE__ */ new Set(), void 0 !== i7.strings && (this.st = new Set(i7.strings.join(" ").split(/\s/).filter((t6) => "" !== t6)));
      for (const t6 in s7)
        s7[t6] && !(null === (r5 = this.st) || void 0 === r5 ? void 0 : r5.has(t6)) && this.nt.add(t6);
      return this.render(s7);
    }
    const e7 = i7.element.classList;
    this.nt.forEach((t6) => {
      t6 in s7 || (e7.remove(t6), this.nt.delete(t6));
    });
    for (const t6 in s7) {
      const i8 = !!s7[t6];
      i8 === this.nt.has(t6) || (null === (o14 = this.st) || void 0 === o14 ? void 0 : o14.has(t6)) || (i8 ? (e7.add(t6), this.nt.add(t6)) : (e7.remove(t6), this.nt.delete(t6)));
    }
    return x;
  }
});

// node_modules/lit-html/directives/guard.js
var e6 = {};
var i4 = e(class extends i {
  constructor() {
    super(...arguments), this.ot = e6;
  }
  render(r5, t6) {
    return t6();
  }
  update(t6, [s7, e7]) {
    if (Array.isArray(s7)) {
      if (Array.isArray(this.ot) && this.ot.length === s7.length && s7.every((r5, t7) => r5 === this.ot[t7]))
        return x;
    } else if (this.ot === s7)
      return x;
    return this.ot = Array.isArray(s7) ? Array.from(s7) : s7, this.render(s7, e7);
  }
});

// node_modules/lit-html/directives/if-defined.js
var l5 = (l7) => null != l7 ? l7 : b;

// node_modules/lit-html/directives/join.js
function* o9(o14, t6) {
  const f2 = "function" == typeof t6;
  if (void 0 !== o14) {
    let i7 = -1;
    for (const n9 of o14)
      i7 > -1 && (yield f2 ? t6(i7) : t6), i7++, yield n9;
  }
}

// node_modules/lit-html/directives/keyed.js
var i5 = e(class extends i {
  constructor() {
    super(...arguments), this.key = b;
  }
  render(r5, t6) {
    return this.key = r5, t6;
  }
  update(r5, [t6, e7]) {
    return t6 !== this.key && (s4(r5), this.key = t6), e7;
  }
});

// node_modules/lit-html/directives/live.js
var l6 = e(class extends i {
  constructor(r5) {
    if (super(r5), r5.type !== t.PROPERTY && r5.type !== t.ATTRIBUTE && r5.type !== t.BOOLEAN_ATTRIBUTE)
      throw Error("The `live` directive is not allowed on child or event bindings");
    if (!e5(r5))
      throw Error("`live` bindings can only contain a single expression");
  }
  render(r5) {
    return r5;
  }
  update(i7, [t6]) {
    if (t6 === x || t6 === b)
      return t6;
    const o14 = i7.element, l7 = i7.name;
    if (i7.type === t.PROPERTY) {
      if (t6 === o14[l7])
        return x;
    } else if (i7.type === t.BOOLEAN_ATTRIBUTE) {
      if (!!t6 === o14.hasAttribute(l7))
        return x;
    } else if (i7.type === t.ATTRIBUTE && o14.getAttribute(l7) === t6 + "")
      return x;
    return s4(i7), t6;
  }
});

// node_modules/lit-html/directives/map.js
function* o10(o14, f2) {
  if (void 0 !== o14) {
    let i7 = 0;
    for (const t6 of o14)
      yield f2(t6, i7++);
  }
}

// node_modules/lit-html/directives/range.js
function* o11(o14, l7, n9 = 1) {
  const t6 = void 0 === l7 ? 0 : o14;
  null != l7 || (l7 = o14);
  for (let o15 = t6; n9 > 0 ? o15 < l7 : l7 < o15; o15 += n9)
    yield o15;
}

// node_modules/lit-html/directives/ref.js
var h4 = /* @__PURE__ */ new WeakMap();
var n6 = e(class extends c3 {
  render(t6) {
    return b;
  }
  update(t6, [s7]) {
    var e7;
    const o14 = s7 !== this.Y;
    return o14 && void 0 !== this.Y && this.rt(void 0), (o14 || this.lt !== this.ct) && (this.Y = s7, this.dt = null === (e7 = t6.options) || void 0 === e7 ? void 0 : e7.host, this.rt(this.ct = t6.element)), b;
  }
  rt(i7) {
    var t6;
    if ("function" == typeof this.Y) {
      const s7 = null !== (t6 = this.dt) && void 0 !== t6 ? t6 : globalThis;
      let e7 = h4.get(s7);
      void 0 === e7 && (e7 = /* @__PURE__ */ new WeakMap(), h4.set(s7, e7)), void 0 !== e7.get(this.Y) && this.Y.call(this.dt, void 0), e7.set(this.Y, i7), void 0 !== i7 && this.Y.call(this.dt, i7);
    } else
      this.Y.value = i7;
  }
  get lt() {
    var i7, t6, s7;
    return "function" == typeof this.Y ? null === (t6 = h4.get(null !== (i7 = this.dt) && void 0 !== i7 ? i7 : globalThis)) || void 0 === t6 ? void 0 : t6.get(this.Y) : null === (s7 = this.Y) || void 0 === s7 ? void 0 : s7.value;
  }
  disconnected() {
    this.lt === this.ct && this.rt(void 0);
  }
  reconnected() {
    this.rt(this.ct);
  }
});

// node_modules/lit-html/directives/repeat.js
var u2 = (e7, s7, t6) => {
  const r5 = /* @__PURE__ */ new Map();
  for (let l7 = s7; l7 <= t6; l7++)
    r5.set(e7[l7], l7);
  return r5;
};
var c5 = e(class extends i {
  constructor(e7) {
    if (super(e7), e7.type !== t.CHILD)
      throw Error("repeat() can only be used in text expressions");
  }
  ht(e7, s7, t6) {
    let r5;
    void 0 === t6 ? t6 = s7 : void 0 !== s7 && (r5 = s7);
    const l7 = [], o14 = [];
    let i7 = 0;
    for (const s8 of e7)
      l7[i7] = r5 ? r5(s8, i7) : i7, o14[i7] = t6(s8, i7), i7++;
    return { values: o14, keys: l7 };
  }
  render(e7, s7, t6) {
    return this.ht(e7, s7, t6).values;
  }
  update(s7, [t6, r5, c7]) {
    var d3;
    const a3 = m(s7), { values: p2, keys: v } = this.ht(t6, r5, c7);
    if (!Array.isArray(a3))
      return this.ut = v, p2;
    const h6 = null !== (d3 = this.ut) && void 0 !== d3 ? d3 : this.ut = [], m2 = [];
    let y2, x2, j = 0, k = a3.length - 1, w = 0, A = p2.length - 1;
    for (; j <= k && w <= A; )
      if (null === a3[j])
        j++;
      else if (null === a3[k])
        k--;
      else if (h6[j] === v[w])
        m2[w] = u(a3[j], p2[w]), j++, w++;
      else if (h6[k] === v[A])
        m2[A] = u(a3[k], p2[A]), k--, A--;
      else if (h6[j] === v[A])
        m2[A] = u(a3[j], p2[A]), r3(s7, m2[A + 1], a3[j]), j++, A--;
      else if (h6[k] === v[w])
        m2[w] = u(a3[k], p2[w]), r3(s7, a3[j], a3[k]), k--, w++;
      else if (void 0 === y2 && (y2 = u2(v, w, A), x2 = u2(h6, j, k)), y2.has(h6[j]))
        if (y2.has(h6[k])) {
          const e7 = x2.get(v[w]), t7 = void 0 !== e7 ? a3[e7] : null;
          if (null === t7) {
            const e8 = r3(s7, a3[j]);
            u(e8, p2[w]), m2[w] = e8;
          } else
            m2[w] = u(t7, p2[w]), r3(s7, a3[j], t7), a3[e7] = null;
          w++;
        } else
          p(a3[k]), k--;
      else
        p(a3[j]), j++;
    for (; w <= A; ) {
      const e7 = r3(s7, m2[A + 1]);
      u(e7, p2[w]), m2[w++] = e7;
    }
    for (; j <= k; ) {
      const e7 = a3[j++];
      null !== e7 && p(e7);
    }
    return this.ut = v, s4(s7, m2), x;
  }
});

// node_modules/lit-html/directives/style-map.js
var i6 = e(class extends i {
  constructor(t6) {
    var e7;
    if (super(t6), t6.type !== t.ATTRIBUTE || "style" !== t6.name || (null === (e7 = t6.strings) || void 0 === e7 ? void 0 : e7.length) > 2)
      throw Error("The `styleMap` directive must be used in the `style` attribute and must be the only part in the attribute.");
  }
  render(t6) {
    return Object.keys(t6).reduce((e7, r5) => {
      const s7 = t6[r5];
      return null == s7 ? e7 : e7 + `${r5 = r5.replace(/(?:^(webkit|moz|ms|o)|)(?=[A-Z])/g, "-$&").toLowerCase()}:${s7};`;
    }, "");
  }
  update(e7, [r5]) {
    const { style: s7 } = e7.element;
    if (void 0 === this.vt) {
      this.vt = /* @__PURE__ */ new Set();
      for (const t6 in r5)
        this.vt.add(t6);
      return this.render(r5);
    }
    this.vt.forEach((t6) => {
      null == r5[t6] && (this.vt.delete(t6), t6.includes("-") ? s7.removeProperty(t6) : s7[t6] = "");
    });
    for (const t6 in r5) {
      const e8 = r5[t6];
      null != e8 && (this.vt.add(t6), t6.includes("-") ? s7.setProperty(t6, e8) : s7[t6] = e8);
    }
    return x;
  }
});

// node_modules/lit-html/directives/template-content.js
var o12 = e(class extends i {
  constructor(t6) {
    if (super(t6), t6.type !== t.CHILD)
      throw Error("templateContent can only be used in child bindings");
  }
  render(r5) {
    return this.ft === r5 ? x : (this.ft = r5, document.importNode(r5.content, true));
  }
});

// node_modules/lit-html/directives/unsafe-svg.js
var t5 = class extends e2 {
};
t5.directiveName = "unsafeSVG", t5.resultType = 2;
var o13 = e(t5);

// node_modules/lit-html/directives/until.js
var n7 = (t6) => !t3(t6) && "function" == typeof t6.then;
var h5 = class extends c3 {
  constructor() {
    super(...arguments), this._$Cwt = 1073741823, this._$Cyt = [], this._$CK = new s6(this), this._$CX = new i3();
  }
  render(...s7) {
    var i7;
    return null !== (i7 = s7.find((t6) => !n7(t6))) && void 0 !== i7 ? i7 : x;
  }
  update(s7, i7) {
    const r5 = this._$Cyt;
    let e7 = r5.length;
    this._$Cyt = i7;
    const o14 = this._$CK, h6 = this._$CX;
    this.isConnected || this.disconnected();
    for (let t6 = 0; t6 < i7.length && !(t6 > this._$Cwt); t6++) {
      const s8 = i7[t6];
      if (!n7(s8))
        return this._$Cwt = t6, s8;
      t6 < e7 && s8 === r5[t6] || (this._$Cwt = 1073741823, e7 = 0, Promise.resolve(s8).then(async (t7) => {
        for (; h6.get(); )
          await h6.get();
        const i8 = o14.deref();
        if (void 0 !== i8) {
          const r6 = i8._$Cyt.indexOf(s8);
          r6 > -1 && r6 < i8._$Cwt && (i8._$Cwt = r6, i8.setValue(t7));
        }
      }));
    }
    return x;
  }
  disconnected() {
    this._$CK.disconnect(), this._$CX.pause();
  }
  reconnected() {
    this._$CK.reconnect(this), this._$CX.resume();
  }
};
var c6 = e(h5);

// node_modules/lit-html/directives/when.js
function n8(n9, o14, r5) {
  return n9 ? o14() : null == r5 ? void 0 : r5();
}

// node_modules/@thomasrandolph/taproot/lib/Component.js
function final(obj, key, value) {
  Object.defineProperty(obj, key, {
    "configurable": false,
    "enumerable": true,
    "writable": false,
    value
  });
}
var Component = class extends s3 {
  constructor(...args) {
    super(...args);
    final(this, "uuid", uuid());
  }
  updated(...args) {
    var changed = args[0];
    super.updated(...args);
    if (this.observe) {
      changed.forEach((oldValue, key) => {
        if (this.observe[key]) {
          this.observe[key](oldValue, this[key]);
        }
      });
    }
  }
  emit(type, detail, init = {}) {
    this.dispatchEvent(new CustomEvent(type, {
      "bubbles": true,
      "cancelable": true,
      "composed": true,
      ...init,
      detail
    }));
  }
  transition(event, key = "stateService") {
    if (this[key]) {
      this[key].send(event);
    }
  }
  is(events, key = "state") {
    if (!(events instanceof Array)) {
      events = [events];
    }
    return events.includes(this[key]);
  }
};

// node_modules/@thomasrandolph/taproot/Component/Component.js
var Component2 = class extends Component {
  static get prefix() {
    return getNamespace();
  }
};
export {
  Component2 as Component,
  c4 as asyncAppend,
  h3 as asyncReplace,
  d2 as cache,
  o7 as choose,
  o8 as classMap,
  i2 as css,
  final,
  i4 as guard,
  y as html,
  l5 as ifDefined,
  o9 as join,
  i5 as keyed,
  l6 as live,
  o10 as map,
  o11 as range,
  n6 as ref,
  Z as render,
  c5 as repeat,
  i6 as styleMap,
  o12 as templateContent,
  r as unsafeCSS,
  o as unsafeHTML,
  o13 as unsafeSVG,
  c6 as until,
  n8 as when
};
/*! Bundled license information:

@lit/reactive-element/css-tag.js:
  (**
   * @license
   * Copyright 2019 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

@lit/reactive-element/reactive-element.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-element/lit-element.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directive-helpers.js:
  (**
   * @license
   * Copyright 2020 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/async-directive.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/private-async-helpers.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/async-replace.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/async-append.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/cache.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/choose.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/class-map.js:
  (**
   * @license
   * Copyright 2018 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/guard.js:
  (**
   * @license
   * Copyright 2018 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/if-defined.js:
  (**
   * @license
   * Copyright 2018 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/join.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/keyed.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/live.js:
  (**
   * @license
   * Copyright 2020 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/map.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/range.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/ref.js:
  (**
   * @license
   * Copyright 2020 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/repeat.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/style-map.js:
  (**
   * @license
   * Copyright 2018 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/template-content.js:
  (**
   * @license
   * Copyright 2020 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/unsafe-svg.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/until.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/when.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)
*/
