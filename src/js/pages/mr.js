import { setup } from "../common/modules/@thomasrandolph/taproot.js";

import { schemas } from "../common/database.js";
import { registerMergeRequestActions } from "../actions/mergeRequests.js";
import { registerAuthenticationActions } from "../actions/authentication.js";

async function registerPageComponents(){
	var App = ( await import( "../components/App/App.js" ) ).App;
	var Authenticate = ( await import( "../components/Authenticate/Authenticate.js" ) ).Authenticate;
	var DiffFile = ( await import( "../components/DiffFile/DiffFile.js" ) ).DiffFile;

	customElements.define( App.as, App );
	customElements.define( Authenticate.as, Authenticate );
	customElements.define( DiffFile.as, DiffFile );
}

export async function start(){
	var mcr = await setup( {
		"namespace": "mcr",
		"routing": false,
		"db": {
			schemas
		}
	} );

	await registerPageComponents();

	registerMergeRequestActions( mcr );
	registerAuthenticationActions( mcr );

	mcr.start();
}
