import "../../../common/MWZFWPW5.js";

// node_modules/@thomasrandolph/taproot/lib/storage/Storage.js
function get(storage, key) {
  return storage.getItem(key);
}
function set(storage, key, val) {
  return storage.setItem(key, val);
}
function remove(storage, key) {
  return storage.removeItem(key);
}

// node_modules/@thomasrandolph/taproot/lib/storage/local.js
function get2(key) {
  return get(localStorage, key);
}
function set2(key, val) {
  return set(localStorage, key, val);
}
function remove2(key) {
  return remove(localStorage, key);
}
export {
  get2 as get,
  remove2 as remove,
  set2 as set
};
