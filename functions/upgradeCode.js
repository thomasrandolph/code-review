// src/js/common/node/helpers.js
import { request } from "https";
function getEnvironmentVariable(name) {
  return process.env[name];
}
function fetch(config2) {
  return new Promise((resolve) => {
    let resBody = [];
    let req = request(
      config2.url,
      config2.options,
      (result) => {
        result.on("data", (data) => {
          resBody.push(data);
        });
        result.on("end", () => {
          resBody = JSON.parse(Buffer.concat(resBody).toString());
          resolve(resBody);
        });
      }
    );
    if (config2.options.method == "POST") {
      req.write(config2.postBody, "utf8");
    }
    req.end();
  });
}

// config.js
var config = {
  "namespace": "mcr",
  "appId": "5477db67ee287de4d264cbbca50af295e2aa32728cddd99fbf512b71ab5b0a6b",
  "appSecretVar": {
    "type": "env",
    "name": "GITLAB_MCR_APP_SECRET"
  }
};

// src/js/common/node/config.js
function getVariable({ type = "config", name, recurse = false } = {}) {
  var methods = {
    "config": (n) => config[n],
    "env": (n) => getEnvironmentVariable(n)
  };
  var result = methods[type](name);
  return recurse ? getVariable(result) : result;
}

// src/functions/upgradeCode.js
async function handler(event) {
  var body = JSON.parse(event.body);
  var postBody = {
    "client_id": getVariable({ "type": "config", "name": "appId" }),
    "client_secret": getVariable({ "type": "config", "name": "appSecretVar", "recurse": true }),
    "code": body.code,
    "grant_type": "authorization_code",
    "redirect_uri": body.redirect
  };
  var config2 = {
    "url": "https://gitlab.com/oauth/token",
    "options": {
      "method": "POST",
      "body": JSON.stringify(postBody),
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
      }
    },
    "postBody": new URLSearchParams(postBody).toString()
  };
  var status = 400;
  var responseBody = JSON.stringify({ "error": "Unknown provider" });
  var auth = await fetch(config2);
  status = 200;
  responseBody = JSON.stringify(auth);
  return {
    "statusCode": status,
    "body": responseBody
  };
}
export {
  handler
};
