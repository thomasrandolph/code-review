// node_modules/@thomasrandolph/taproot/lib/Number.js
function clamp(source, min, max) {
  return Math.max(Math.min(max, source), min);
}
function stringToNumberHash(input) {
  var strHash = 5381;
  var i = input.length;
  while (i) {
    strHash = strHash * 33 ^ input.charCodeAt(--i);
  }
  return strHash >>> 0;
}

export {
  clamp,
  stringToNumberHash
};
