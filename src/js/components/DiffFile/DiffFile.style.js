import { css } from "../../common/modules/@thomasrandolph/taproot/Component.js";

export function style(){
	return css`

	@layer component;
	@layer component.vendor, component.base;

	@layer component.vendor{
		/* highlight.js monokai-sublime.min.css */
		pre code.hljs{display:block;overflow-x:auto;padding:1em}code.hljs{padding:3px 5px}.hljs{background:#23241f;color:#f8f8f2}.hljs-subst,.hljs-tag{color:#f8f8f2}.hljs-emphasis,.hljs-strong{color:#a8a8a2}.hljs-bullet,.hljs-link,.hljs-literal,.hljs-number,.hljs-quote,.hljs-regexp{color:#ae81ff}.hljs-code,.hljs-section,.hljs-selector-class,.hljs-title{color:#a6e22e}.hljs-strong{font-weight:700}.hljs-emphasis{font-style:italic}.hljs-attr,.hljs-keyword,.hljs-name,.hljs-selector-tag{color:#f92672}.hljs-attribute,.hljs-symbol{color:#66d9ef}.hljs-class .hljs-title,.hljs-params,.hljs-title.class_{color:#f8f8f2}.hljs-addition,.hljs-built_in,.hljs-selector-attr,.hljs-selector-id,.hljs-selector-pseudo,.hljs-string,.hljs-template-variable,.hljs-type,.hljs-variable{color:#e6db74}.hljs-comment,.hljs-deletion,.hljs-meta{color:#75715e}
	}

	@layer component.base{
		div[file]{
			border: 1px solid gray;
			border-radius: 3px;
			min-width: 500px;
			width: 75vw;
		}

		div[file] div[header]{
			border-block-end: 1px solid gray;
			padding: 1rem;
		}

		pre{
			font-size: 1.5rem;
			line-height: 1.1;
			margin: 0;
			position: relative;
		}

		pre code.hljs{
			background: transparent;
			padding-block-start: 0;
			position: relative;
			z-index: 1;
		}

		pre > div{
			background-color: #23241f;
			height: 100%;
			left: 0;
			position: absolute;
			top: 0;
			width: 100%;
			z-index: 0;
		}

		pre > div div[addition]{
			background-color: hsla( 120, 100%, 75%, .1 );
		}

		pre > div div[deletion]{
			background-color: hsla( 0, 100%, 75%, .1 );
		}

		pre > div div[match]{
			background-color: hsla( 0, 0%, 25%, 1 );
		}

		pre code{
			font-family: 'Sono', sans-serif;
		}
	}

	`;
}
