import{b as ht,g as dt}from"./6A2ISCT3.js";var W,j=window,T=j.trustedTypes,ct=T?T.createPolicy("lit-html",{createHTML:t=>t}):void 0,C=`lit$${(Math.random()+"").slice(9)}$`,X="?"+C,qt=`<${X}>`,P=document,H=(t="")=>P.createComment(t),O=t=>t===null||typeof t!="object"&&typeof t!="function",At=Array.isArray,yt=t=>At(t)||typeof t?.[Symbol.iterator]=="function",N=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,ut=/-->/g,vt=/>/g,w=RegExp(`>|[ 	
\f\r](?:([^\\s"'>=/]+)([ 	
\f\r]*=[ 	
\f\r]*(?:[^ 	
\f\r"'\`<>=]|("|')|))|$)`,"g"),pt=/'/g,ft=/"/g,mt=/^(?:script|style|textarea|title)$/i,gt=t=>(e,...s)=>({_$litType$:t,strings:e,values:s}),Jt=gt(1),ve=gt(2),f=Symbol.for("lit-noChange"),p=Symbol.for("lit-nothing"),$t=new WeakMap,x=P.createTreeWalker(P,129,null,!1),Ct=(t,e)=>{let s=t.length-1,i=[],r,n=e===2?"<svg>":"",o=N;for(let l=0;l<s;l++){let h=t[l],u,c,d=-1,$=0;for(;$<h.length&&(o.lastIndex=$,c=o.exec(h),c!==null);)$=o.lastIndex,o===N?c[1]==="!--"?o=ut:c[1]!==void 0?o=vt:c[2]!==void 0?(mt.test(c[2])&&(r=RegExp("</"+c[2],"g")),o=w):c[3]!==void 0&&(o=w):o===w?c[0]===">"?(o=r??N,d=-1):c[1]===void 0?d=-2:(d=o.lastIndex-c[2].length,u=c[1],o=c[3]===void 0?w:c[3]==='"'?ft:pt):o===ft||o===pt?o=w:o===ut||o===vt?o=N:(o=w,r=void 0);let v=o===w&&t[l+1].startsWith("/>")?" ":"";n+=o===N?h+qt:d>=0?(i.push(u),h.slice(0,d)+"$lit$"+h.slice(d)+C+v):h+C+(d===-2?(i.push(void 0),l):v)}let a=n+(t[s]||"<?>")+(e===2?"</svg>":"");if(!Array.isArray(t)||!t.hasOwnProperty("raw"))throw Error("invalid template strings array");return[ct!==void 0?ct.createHTML(a):a,i]},D=class{constructor({strings:t,_$litType$:e},s){let i;this.parts=[];let r=0,n=0,o=t.length-1,a=this.parts,[l,h]=Ct(t,e);if(this.el=D.createElement(l,s),x.currentNode=this.el.content,e===2){let u=this.el.content,c=u.firstChild;c.remove(),u.append(...c.childNodes)}for(;(i=x.nextNode())!==null&&a.length<o;){if(i.nodeType===1){if(i.hasAttributes()){let u=[];for(let c of i.getAttributeNames())if(c.endsWith("$lit$")||c.startsWith(C)){let d=h[n++];if(u.push(c),d!==void 0){let $=i.getAttribute(d.toLowerCase()+"$lit$").split(C),v=/([.?@])?(.*)/.exec(d);a.push({type:1,index:r,name:v[2],strings:$,ctor:v[1]==="."?bt:v[1]==="?"?wt:v[1]==="@"?St:R})}else a.push({type:6,index:r})}for(let c of u)i.removeAttribute(c)}if(mt.test(i.tagName)){let u=i.textContent.split(C),c=u.length-1;if(c>0){i.textContent=T?T.emptyScript:"";for(let d=0;d<c;d++)i.append(u[d],H()),x.nextNode(),a.push({type:2,index:++r});i.append(u[c],H())}}}else if(i.nodeType===8)if(i.data===X)a.push({type:2,index:r});else{let u=-1;for(;(u=i.data.indexOf(C,u+1))!==-1;)a.push({type:7,index:r}),u+=C.length-1}r++}}static createElement(t,e){let s=P.createElement("template");return s.innerHTML=t,s}};function S(t,e,s=t,i){var r,n,o,a;if(e===f)return e;let l=i!==void 0?(r=s._$Co)===null||r===void 0?void 0:r[i]:s._$Cl,h=O(e)?void 0:e._$litDirective$;return l?.constructor!==h&&((n=l?._$AO)===null||n===void 0||n.call(l,!1),h===void 0?l=void 0:(l=new h(t),l._$AT(t,s,i)),i!==void 0?((o=(a=s)._$Co)!==null&&o!==void 0?o:a._$Co=[])[i]=l:s._$Cl=l),l!==void 0&&(e=S(t,l._$AS(t,e.values),l,i)),e}var Et=class{constructor(t,e){this.u=[],this._$AN=void 0,this._$AD=t,this._$AM=e}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}v(t){var e;let{el:{content:s},parts:i}=this._$AD,r=((e=t?.creationScope)!==null&&e!==void 0?e:P).importNode(s,!0);x.currentNode=r;let n=x.nextNode(),o=0,a=0,l=i[0];for(;l!==void 0;){if(o===l.index){let h;l.type===2?h=new M(n,n.nextSibling,this,t):l.type===1?h=new l.ctor(n,l.name,l.strings,this,t):l.type===6&&(h=new xt(n,this,t)),this.u.push(h),l=i[++a]}o!==l?.index&&(n=x.nextNode(),o++)}return r}p(t){let e=0;for(let s of this.u)s!==void 0&&(s.strings!==void 0?(s._$AI(t,s,e),e+=s.strings.length-2):s._$AI(t[e])),e++}},M=class{constructor(t,e,s,i){var r;this.type=2,this._$AH=p,this._$AN=void 0,this._$AA=t,this._$AB=e,this._$AM=s,this.options=i,this._$Cm=(r=i?.isConnected)===null||r===void 0||r}get _$AU(){var t,e;return(e=(t=this._$AM)===null||t===void 0?void 0:t._$AU)!==null&&e!==void 0?e:this._$Cm}get parentNode(){let t=this._$AA.parentNode,e=this._$AM;return e!==void 0&&t.nodeType===11&&(t=e.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,e=this){t=S(this,t,e),O(t)?t===p||t==null||t===""?(this._$AH!==p&&this._$AR(),this._$AH=p):t!==this._$AH&&t!==f&&this.g(t):t._$litType$!==void 0?this.$(t):t.nodeType!==void 0?this.T(t):yt(t)?this.k(t):this.g(t)}O(t,e=this._$AB){return this._$AA.parentNode.insertBefore(t,e)}T(t){this._$AH!==t&&(this._$AR(),this._$AH=this.O(t))}g(t){this._$AH!==p&&O(this._$AH)?this._$AA.nextSibling.data=t:this.T(P.createTextNode(t)),this._$AH=t}$(t){var e;let{values:s,_$litType$:i}=t,r=typeof i=="number"?this._$AC(t):(i.el===void 0&&(i.el=D.createElement(i.h,this.options)),i);if(((e=this._$AH)===null||e===void 0?void 0:e._$AD)===r)this._$AH.p(s);else{let n=new Et(r,this),o=n.v(this.options);n.p(s),this.T(o),this._$AH=n}}_$AC(t){let e=$t.get(t.strings);return e===void 0&&$t.set(t.strings,e=new D(t)),e}k(t){At(this._$AH)||(this._$AH=[],this._$AR());let e=this._$AH,s,i=0;for(let r of t)i===e.length?e.push(s=new M(this.O(H()),this.O(H()),this,this.options)):s=e[i],s._$AI(r),i++;i<e.length&&(this._$AR(s&&s._$AB.nextSibling,i),e.length=i)}_$AR(t=this._$AA.nextSibling,e){var s;for((s=this._$AP)===null||s===void 0||s.call(this,!1,!0,e);t&&t!==this._$AB;){let i=t.nextSibling;t.remove(),t=i}}setConnected(t){var e;this._$AM===void 0&&(this._$Cm=t,(e=this._$AP)===null||e===void 0||e.call(this,t))}},R=class{constructor(t,e,s,i,r){this.type=1,this._$AH=p,this._$AN=void 0,this.element=t,this.name=e,this._$AM=i,this.options=r,s.length>2||s[0]!==""||s[1]!==""?(this._$AH=Array(s.length-1).fill(new String),this.strings=s):this._$AH=p}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,e=this,s,i){let r=this.strings,n=!1;if(r===void 0)t=S(this,t,e,0),n=!O(t)||t!==this._$AH&&t!==f,n&&(this._$AH=t);else{let o=t,a,l;for(t=r[0],a=0;a<r.length-1;a++)l=S(this,o[s+a],e,a),l===f&&(l=this._$AH[a]),n||(n=!O(l)||l!==this._$AH[a]),l===p?t=p:t!==p&&(t+=(l??"")+r[a+1]),this._$AH[a]=l}n&&!i&&this.j(t)}j(t){t===p?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,t??"")}},bt=class extends R{constructor(){super(...arguments),this.type=3}j(t){this.element[this.name]=t===p?void 0:t}},Qt=T?T.emptyScript:"",wt=class extends R{constructor(){super(...arguments),this.type=4}j(t){t&&t!==p?this.element.setAttribute(this.name,Qt):this.element.removeAttribute(this.name)}},St=class extends R{constructor(t,e,s,i,r){super(t,e,s,i,r),this.type=5}_$AI(t,e=this){var s;if((t=(s=S(this,t,e,0))!==null&&s!==void 0?s:p)===f)return;let i=this._$AH,r=t===p&&i!==p||t.capture!==i.capture||t.once!==i.once||t.passive!==i.passive,n=t!==p&&(i===p||r);r&&this.element.removeEventListener(this.name,this,i),n&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var e,s;typeof this._$AH=="function"?this._$AH.call((s=(e=this.options)===null||e===void 0?void 0:e.host)!==null&&s!==void 0?s:this.element,t):this._$AH.handleEvent(t)}},xt=class{constructor(t,e,s){this.element=t,this.type=6,this._$AN=void 0,this._$AM=e,this.options=s}get _$AU(){return this._$AM._$AU}_$AI(t){S(this,t)}},Tt={P:"$lit$",A:C,M:X,C:1,L:Ct,R:Et,D:yt,V:S,I:M,H:R,N:wt,U:St,B:bt,F:xt},_t=j.litHtmlPolyfillSupport;_t?.(D,M),((W=j.litHtmlVersions)!==null&&W!==void 0?W:j.litHtmlVersions=[]).push("2.6.1");var q=(t,e,s)=>{var i,r;let n=(i=s?.renderBefore)!==null&&i!==void 0?i:e,o=n._$litPart$;if(o===void 0){let a=(r=s?.renderBefore)!==null&&r!==void 0?r:null;n._$litPart$=o=new M(e.insertBefore(H(),a),a,void 0,s??{})}return o._$AI(t),o};var A={ATTRIBUTE:1,CHILD:2,PROPERTY:3,BOOLEAN_ATTRIBUTE:4,EVENT:5,ELEMENT:6},_=t=>(...e)=>({_$litDirective$:t,values:e}),m=class{constructor(t){}get _$AU(){return this._$AM._$AU}_$AT(t,e,s){this._$Ct=t,this._$AM=e,this._$Ci=s}_$AS(t,e){return this.update(t,e)}update(t,e){return this.render(...e)}},k=class extends m{constructor(t){if(super(t),this.it=p,t.type!==A.CHILD)throw Error(this.constructor.directiveName+"() can only be used in child bindings")}render(t){if(t===p||t==null)return this._t=void 0,this.it=t;if(t===f)return t;if(typeof t!="string")throw Error(this.constructor.directiveName+"() called with a non-string value");if(t===this.it)return this._t;this.it=t;let e=[t];return e.raw=e,this._t={_$litType$:this.constructor.resultType,strings:e,values:[]}}};k.directiveName="unsafeHTML",k.resultType=1;var Ft=_(k);var z=window,nt=z.ShadowRoot&&(z.ShadyCSS===void 0||z.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,ot=Symbol(),Pt=new WeakMap,It=class{constructor(t,e,s){if(this._$cssResult$=!0,s!==ot)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t,this.t=e}get styleSheet(){let t=this.o,e=this.t;if(nt&&t===void 0){let s=e!==void 0&&e.length===1;s&&(t=Pt.get(e)),t===void 0&&((this.o=t=new CSSStyleSheet).replaceSync(this.cssText),s&&Pt.set(e,t))}return t}toString(){return this.cssText}},Gt=t=>new It(typeof t=="string"?t:t+"",void 0,ot),Lt=(t,...e)=>{let s=t.length===1?t[0]:e.reduce((i,r,n)=>i+(o=>{if(o._$cssResult$===!0)return o.cssText;if(typeof o=="number")return o;throw Error("Value passed to 'css' function must be a 'css' function result: "+o+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(r)+t[n+1],t[0]);return new It(s,t,ot)},te=(t,e)=>{nt?t.adoptedStyleSheets=e.map(s=>s instanceof CSSStyleSheet?s:s.styleSheet):e.forEach(s=>{let i=document.createElement("style"),r=z.litNonce;r!==void 0&&i.setAttribute("nonce",r),i.textContent=s.cssText,t.appendChild(i)})},Ut=nt?t=>t:t=>t instanceof CSSStyleSheet?(e=>{let s="";for(let i of e.cssRules)s+=i.cssText;return Gt(s)})(t):t,J,V=window,Nt=V.trustedTypes,ee=Nt?Nt.emptyScript:"",Ht=V.reactiveElementPolyfillSupport,st={toAttribute(t,e){switch(e){case Boolean:t=t?ee:null;break;case Object:case Array:t=t==null?t:JSON.stringify(t)}return t},fromAttribute(t,e){let s=t;switch(e){case Boolean:s=t!==null;break;case Number:s=t===null?null:Number(t);break;case Object:case Array:try{s=JSON.parse(t)}catch{s=null}}return s}},Bt=(t,e)=>e!==t&&(e==e||t==t),Q={attribute:!0,type:String,converter:st,reflect:!1,hasChanged:Bt},U=class extends HTMLElement{constructor(){super(),this._$Ei=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$El=null,this.u()}static addInitializer(t){var e;this.finalize(),((e=this.h)!==null&&e!==void 0?e:this.h=[]).push(t)}static get observedAttributes(){this.finalize();let t=[];return this.elementProperties.forEach((e,s)=>{let i=this._$Ep(s,e);i!==void 0&&(this._$Ev.set(i,s),t.push(i))}),t}static createProperty(t,e=Q){if(e.state&&(e.attribute=!1),this.finalize(),this.elementProperties.set(t,e),!e.noAccessor&&!this.prototype.hasOwnProperty(t)){let s=typeof t=="symbol"?Symbol():"__"+t,i=this.getPropertyDescriptor(t,s,e);i!==void 0&&Object.defineProperty(this.prototype,t,i)}}static getPropertyDescriptor(t,e,s){return{get(){return this[e]},set(i){let r=this[t];this[e]=i,this.requestUpdate(t,r,s)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||Q}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;let t=Object.getPrototypeOf(this);if(t.finalize(),t.h!==void 0&&(this.h=[...t.h]),this.elementProperties=new Map(t.elementProperties),this._$Ev=new Map,this.hasOwnProperty("properties")){let e=this.properties,s=[...Object.getOwnPropertyNames(e),...Object.getOwnPropertySymbols(e)];for(let i of s)this.createProperty(i,e[i])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){let e=[];if(Array.isArray(t)){let s=new Set(t.flat(1/0).reverse());for(let i of s)e.unshift(Ut(i))}else t!==void 0&&e.push(Ut(t));return e}static _$Ep(t,e){let s=e.attribute;return s===!1?void 0:typeof s=="string"?s:typeof t=="string"?t.toLowerCase():void 0}u(){var t;this._$E_=new Promise(e=>this.enableUpdating=e),this._$AL=new Map,this._$Eg(),this.requestUpdate(),(t=this.constructor.h)===null||t===void 0||t.forEach(e=>e(this))}addController(t){var e,s;((e=this._$ES)!==null&&e!==void 0?e:this._$ES=[]).push(t),this.renderRoot!==void 0&&this.isConnected&&((s=t.hostConnected)===null||s===void 0||s.call(t))}removeController(t){var e;(e=this._$ES)===null||e===void 0||e.splice(this._$ES.indexOf(t)>>>0,1)}_$Eg(){this.constructor.elementProperties.forEach((t,e)=>{this.hasOwnProperty(e)&&(this._$Ei.set(e,this[e]),delete this[e])})}createRenderRoot(){var t;let e=(t=this.shadowRoot)!==null&&t!==void 0?t:this.attachShadow(this.constructor.shadowRootOptions);return te(e,this.constructor.elementStyles),e}connectedCallback(){var t;this.renderRoot===void 0&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),(t=this._$ES)===null||t===void 0||t.forEach(e=>{var s;return(s=e.hostConnected)===null||s===void 0?void 0:s.call(e)})}enableUpdating(t){}disconnectedCallback(){var t;(t=this._$ES)===null||t===void 0||t.forEach(e=>{var s;return(s=e.hostDisconnected)===null||s===void 0?void 0:s.call(e)})}attributeChangedCallback(t,e,s){this._$AK(t,s)}_$EO(t,e,s=Q){var i;let r=this.constructor._$Ep(t,s);if(r!==void 0&&s.reflect===!0){let n=(((i=s.converter)===null||i===void 0?void 0:i.toAttribute)!==void 0?s.converter:st).toAttribute(e,s.type);this._$El=t,n==null?this.removeAttribute(r):this.setAttribute(r,n),this._$El=null}}_$AK(t,e){var s;let i=this.constructor,r=i._$Ev.get(t);if(r!==void 0&&this._$El!==r){let n=i.getPropertyOptions(r),o=typeof n.converter=="function"?{fromAttribute:n.converter}:((s=n.converter)===null||s===void 0?void 0:s.fromAttribute)!==void 0?n.converter:st;this._$El=r,this[r]=o.fromAttribute(e,n.type),this._$El=null}}requestUpdate(t,e,s){let i=!0;t!==void 0&&(((s=s||this.constructor.getPropertyOptions(t)).hasChanged||Bt)(this[t],e)?(this._$AL.has(t)||this._$AL.set(t,e),s.reflect===!0&&this._$El!==t&&(this._$EC===void 0&&(this._$EC=new Map),this._$EC.set(t,s))):i=!1),!this.isUpdatePending&&i&&(this._$E_=this._$Ej())}async _$Ej(){this.isUpdatePending=!0;try{await this._$E_}catch(e){Promise.reject(e)}let t=this.scheduleUpdate();return t!=null&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Ei&&(this._$Ei.forEach((i,r)=>this[r]=i),this._$Ei=void 0);let e=!1,s=this._$AL;try{e=this.shouldUpdate(s),e?(this.willUpdate(s),(t=this._$ES)===null||t===void 0||t.forEach(i=>{var r;return(r=i.hostUpdate)===null||r===void 0?void 0:r.call(i)}),this.update(s)):this._$Ek()}catch(i){throw e=!1,this._$Ek(),i}e&&this._$AE(s)}willUpdate(t){}_$AE(t){var e;(e=this._$ES)===null||e===void 0||e.forEach(s=>{var i;return(i=s.hostUpdated)===null||i===void 0?void 0:i.call(s)}),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$Ek(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$E_}shouldUpdate(t){return!0}update(t){this._$EC!==void 0&&(this._$EC.forEach((e,s)=>this._$EO(s,this[s],e)),this._$EC=void 0),this._$Ek()}updated(t){}firstUpdated(t){}};U.finalized=!0,U.elementProperties=new Map,U.elementStyles=[],U.shadowRootOptions={mode:"open"},Ht?.({ReactiveElement:U}),((J=V.reactiveElementVersions)!==null&&J!==void 0?J:V.reactiveElementVersions=[]).push("1.6.1");var F,G,I=class extends U{constructor(){super(...arguments),this.renderOptions={host:this},this._$Do=void 0}createRenderRoot(){var t,e;let s=super.createRenderRoot();return(t=(e=this.renderOptions).renderBefore)!==null&&t!==void 0||(e.renderBefore=s.firstChild),s}update(t){let e=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Do=q(e,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),(t=this._$Do)===null||t===void 0||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),(t=this._$Do)===null||t===void 0||t.setConnected(!1)}render(){return f}};I.finalized=!0,I._$litElement$=!0,(F=globalThis.litElementHydrateSupport)===null||F===void 0||F.call(globalThis,{LitElement:I});var Ot=globalThis.litElementPolyfillSupport;Ot?.({LitElement:I});((G=globalThis.litElementVersions)!==null&&G!==void 0?G:globalThis.litElementVersions=[]).push("3.2.2");var{I:se}=Tt,ie=t=>t===null||typeof t!="object"&&typeof t!="function",Y=(t,e)=>e===void 0?t?._$litType$!==void 0:t?._$litType$===e,jt=t=>t.strings===void 0,Mt=()=>document.createComment(""),b=(t,e,s)=>{var i;let r=t._$AA.parentNode,n=e===void 0?t._$AB:e._$AA;if(s===void 0){let o=r.insertBefore(Mt(),n),a=r.insertBefore(Mt(),n);s=new se(o,a,t,t.options)}else{let o=s._$AB.nextSibling,a=s._$AM,l=a!==t;if(l){let h;(i=s._$AQ)===null||i===void 0||i.call(s,t),s._$AM=t,s._$AP!==void 0&&(h=t._$AU)!==a._$AU&&s._$AP(h)}if(o!==n||l){let h=s._$AA;for(;h!==o;){let u=h.nextSibling;r.insertBefore(h,n),h=u}}}return s},E=(t,e,s=t)=>(t._$AI(e,s),t),re={},B=(t,e=re)=>t._$AH=e,it=t=>t._$AH,tt=t=>{var e;(e=t._$AP)===null||e===void 0||e.call(t,!1,!0);let s=t._$AA,i=t._$AB.nextSibling;for(;s!==i;){let r=s.nextSibling;s.remove(),s=r}},Dt=t=>{t._$AR()},L=(t,e)=>{var s,i;let r=t._$AN;if(r===void 0)return!1;for(let n of r)(i=(s=n)._$AO)===null||i===void 0||i.call(s,e,!1),L(n,e);return!0},K=t=>{let e,s;do{if((e=t._$AM)===void 0)break;s=e._$AN,s.delete(t),t=e}while(s?.size===0)},Yt=t=>{for(let e;e=t._$AM;t=e){let s=e._$AN;if(s===void 0)e._$AN=s=new Set;else if(s.has(t))break;s.add(t),le(e)}};function ne(t){this._$AN!==void 0?(K(this),this._$AM=t,Yt(this)):this._$AM=t}function oe(t,e=!1,s=0){let i=this._$AH,r=this._$AN;if(r!==void 0&&r.size!==0)if(e)if(Array.isArray(i))for(let n=s;n<i.length;n++)L(i[n],!1),K(i[n]);else i!=null&&(L(i,!1),K(i));else L(this,t)}var le=t=>{var e,s,i,r;t.type==A.CHILD&&((e=(i=t)._$AP)!==null&&e!==void 0||(i._$AP=oe),(s=(r=t)._$AQ)!==null&&s!==void 0||(r._$AQ=ne))},lt=class extends m{constructor(){super(...arguments),this._$AN=void 0}_$AT(t,e,s){super._$AT(t,e,s),Yt(this),this.isConnected=t._$AU}_$AO(t,e=!0){var s,i;t!==this.isConnected&&(this.isConnected=t,t?(s=this.reconnected)===null||s===void 0||s.call(this):(i=this.disconnected)===null||i===void 0||i.call(this)),e&&(L(this,t),K(this))}setValue(t){if(jt(this._$Ct))this._$Ct._$AI(t,this);else{let e=[...this._$Ct._$AH];e[this._$Ci]=t,this._$Ct._$AI(e,this,0)}}disconnected(){}reconnected(){}},ae=async(t,e)=>{for await(let s of t)if(await e(s)===!1)return},zt=class{constructor(t){this.Y=t}disconnect(){this.Y=void 0}reconnect(t){this.Y=t}deref(){return this.Y}},Vt=class{constructor(){this.Z=void 0,this.q=void 0}get(){return this.Z}pause(){var t;(t=this.Z)!==null&&t!==void 0||(this.Z=new Promise(e=>this.q=e))}resume(){var t;(t=this.q)===null||t===void 0||t.call(this),this.Z=this.q=void 0}},Kt=class extends lt{constructor(){super(...arguments),this._$CK=new zt(this),this._$CX=new Vt}render(t,e){return f}update(t,[e,s]){if(this.isConnected||this.disconnected(),e===this._$CJ)return;this._$CJ=e;let i=0,{_$CK:r,_$CX:n}=this;return ae(e,async o=>{for(;n.get();)await n.get();let a=r.deref();if(a!==void 0){if(a._$CJ!==e)return!1;s!==void 0&&(o=s(o,i)),a.commitValue(o,i),i++}return!0}),f}commitValue(t,e){this.setValue(t)}disconnected(){this._$CK.disconnect(),this._$CX.pause()}reconnected(){this._$CK.reconnect(this),this._$CX.resume()}},Oe=_(Kt),Me=_(class extends Kt{constructor(t){if(super(t),t.type!==A.CHILD)throw Error("asyncAppend can only be used in child expressions")}update(t,e){return this._$Ctt=t,super.update(t,e)}commitValue(t,e){e===0&&Dt(this._$Ctt);let s=b(this._$Ctt);E(s,t)}}),Re=_(class extends m{constructor(t){super(t),this.et=new WeakMap}render(t){return[t]}update(t,[e]){if(Y(this.it)&&(!Y(e)||this.it.strings!==e.strings)){let s=it(t).pop(),i=this.et.get(this.it.strings);if(i===void 0){let r=document.createDocumentFragment();i=q(p,r),i.setConnected(!1),this.et.set(this.it.strings,i)}B(i,[s]),b(i,void 0,s)}if(Y(e)){if(!Y(this.it)||this.it.strings!==e.strings){let s=this.et.get(e.strings);if(s!==void 0){let i=it(s).pop();Dt(t),b(t,void 0,i),B(t,[i])}}this.it=e}else this.it=void 0;return this.render(e)}});var ke=_(class extends m{constructor(t){var e;if(super(t),t.type!==A.ATTRIBUTE||t.name!=="class"||((e=t.strings)===null||e===void 0?void 0:e.length)>2)throw Error("`classMap()` can only be used in the `class` attribute and must be the only part in the attribute.")}render(t){return" "+Object.keys(t).filter(e=>t[e]).join(" ")+" "}update(t,[e]){var s,i;if(this.nt===void 0){this.nt=new Set,t.strings!==void 0&&(this.st=new Set(t.strings.join(" ").split(/\s/).filter(n=>n!=="")));for(let n in e)e[n]&&!(!((s=this.st)===null||s===void 0)&&s.has(n))&&this.nt.add(n);return this.render(e)}let r=t.element.classList;this.nt.forEach(n=>{n in e||(r.remove(n),this.nt.delete(n))});for(let n in e){let o=!!e[n];o===this.nt.has(n)||!((i=this.st)===null||i===void 0)&&i.has(n)||(o?(r.add(n),this.nt.add(n)):(r.remove(n),this.nt.delete(n)))}return f}}),he={},Ie=_(class extends m{constructor(){super(...arguments),this.ot=he}render(t,e){return e()}update(t,[e,s]){if(Array.isArray(e)){if(Array.isArray(this.ot)&&this.ot.length===e.length&&e.every((i,r)=>i===this.ot[r]))return f}else if(this.ot===e)return f;return this.ot=Array.isArray(e)?Array.from(e):e,this.render(e,s)}});var Le=_(class extends m{constructor(){super(...arguments),this.key=p}render(t,e){return this.key=t,e}update(t,[e,s]){return e!==this.key&&(B(t),this.key=e),s}}),Be=_(class extends m{constructor(t){if(super(t),t.type!==A.PROPERTY&&t.type!==A.ATTRIBUTE&&t.type!==A.BOOLEAN_ATTRIBUTE)throw Error("The `live` directive is not allowed on child or event bindings");if(!jt(t))throw Error("`live` bindings can only contain a single expression")}render(t){return t}update(t,[e]){if(e===f||e===p)return e;let s=t.element,i=t.name;if(t.type===A.PROPERTY){if(e===s[i])return f}else if(t.type===A.BOOLEAN_ATTRIBUTE){if(!!e===s.hasAttribute(i))return f}else if(t.type===A.ATTRIBUTE&&s.getAttribute(i)===e+"")return f;return B(t),e}});var et=new WeakMap,je=_(class extends lt{render(t){return p}update(t,[e]){var s;let i=e!==this.Y;return i&&this.Y!==void 0&&this.rt(void 0),(i||this.lt!==this.ct)&&(this.Y=e,this.dt=(s=t.options)===null||s===void 0?void 0:s.host,this.rt(this.ct=t.element)),p}rt(t){var e;if(typeof this.Y=="function"){let s=(e=this.dt)!==null&&e!==void 0?e:globalThis,i=et.get(s);i===void 0&&(i=new WeakMap,et.set(s,i)),i.get(this.Y)!==void 0&&this.Y.call(this.dt,void 0),i.set(this.Y,t),t!==void 0&&this.Y.call(this.dt,t)}else this.Y.value=t}get lt(){var t,e,s;return typeof this.Y=="function"?(e=et.get((t=this.dt)!==null&&t!==void 0?t:globalThis))===null||e===void 0?void 0:e.get(this.Y):(s=this.Y)===null||s===void 0?void 0:s.value}disconnected(){this.lt===this.ct&&this.rt(void 0)}reconnected(){this.rt(this.ct)}}),Rt=(t,e,s)=>{let i=new Map;for(let r=e;r<=s;r++)i.set(t[r],r);return i},De=_(class extends m{constructor(t){if(super(t),t.type!==A.CHILD)throw Error("repeat() can only be used in text expressions")}ht(t,e,s){let i;s===void 0?s=e:e!==void 0&&(i=e);let r=[],n=[],o=0;for(let a of t)r[o]=i?i(a,o):o,n[o]=s(a,o),o++;return{values:n,keys:r}}render(t,e,s){return this.ht(t,e,s).values}update(t,[e,s,i]){var r;let n=it(t),{values:o,keys:a}=this.ht(e,s,i);if(!Array.isArray(n))return this.ut=a,o;let l=(r=this.ut)!==null&&r!==void 0?r:this.ut=[],h=[],u,c,d=0,$=n.length-1,v=0,y=o.length-1;for(;d<=$&&v<=y;)if(n[d]===null)d++;else if(n[$]===null)$--;else if(l[d]===a[v])h[v]=E(n[d],o[v]),d++,v++;else if(l[$]===a[y])h[y]=E(n[$],o[y]),$--,y--;else if(l[d]===a[y])h[y]=E(n[d],o[y]),b(t,h[y+1],n[d]),d++,y--;else if(l[$]===a[v])h[v]=E(n[$],o[v]),b(t,n[d],n[$]),$--,v++;else if(u===void 0&&(u=Rt(a,v,y),c=Rt(l,d,$)),u.has(l[d]))if(u.has(l[$])){let g=c.get(a[v]),Z=g!==void 0?n[g]:null;if(Z===null){let at=b(t,n[d]);E(at,o[v]),h[v]=at}else h[v]=E(Z,o[v]),b(t,n[d],Z),n[g]=null;v++}else tt(n[$]),$--;else tt(n[d]),d++;for(;v<=y;){let g=b(t,h[y+1]);E(g,o[v]),h[v++]=g}for(;d<=$;){let g=n[d++];g!==null&&tt(g)}return this.ut=a,B(t,h),f}}),Ye=_(class extends m{constructor(t){var e;if(super(t),t.type!==A.ATTRIBUTE||t.name!=="style"||((e=t.strings)===null||e===void 0?void 0:e.length)>2)throw Error("The `styleMap` directive must be used in the `style` attribute and must be the only part in the attribute.")}render(t){return Object.keys(t).reduce((e,s)=>{let i=t[s];return i==null?e:e+`${s=s.replace(/(?:^(webkit|moz|ms|o)|)(?=[A-Z])/g,"-$&").toLowerCase()}:${i};`},"")}update(t,[e]){let{style:s}=t.element;if(this.vt===void 0){this.vt=new Set;for(let i in e)this.vt.add(i);return this.render(e)}this.vt.forEach(i=>{e[i]==null&&(this.vt.delete(i),i.includes("-")?s.removeProperty(i):s[i]="")});for(let i in e){let r=e[i];r!=null&&(this.vt.add(i),i.includes("-")?s.setProperty(i,r):s[i]=r)}return f}}),ze=_(class extends m{constructor(t){if(super(t),t.type!==A.CHILD)throw Error("templateContent can only be used in child bindings")}render(t){return this.ft===t?f:(this.ft=t,document.importNode(t.content,!0))}}),rt=class extends k{};rt.directiveName="unsafeSVG",rt.resultType=2;var Ve=_(rt),kt=t=>!ie(t)&&typeof t.then=="function",de=class extends lt{constructor(){super(...arguments),this._$Cwt=1073741823,this._$Cyt=[],this._$CK=new zt(this),this._$CX=new Vt}render(...t){var e;return(e=t.find(s=>!kt(s)))!==null&&e!==void 0?e:f}update(t,e){let s=this._$Cyt,i=s.length;this._$Cyt=e;let r=this._$CK,n=this._$CX;this.isConnected||this.disconnected();for(let o=0;o<e.length&&!(o>this._$Cwt);o++){let a=e[o];if(!kt(a))return this._$Cwt=o,a;o<i&&a===s[o]||(this._$Cwt=1073741823,i=0,Promise.resolve(a).then(async l=>{for(;n.get();)await n.get();let h=r.deref();if(h!==void 0){let u=h._$Cyt.indexOf(a);u>-1&&u<h._$Cwt&&(h._$Cwt=u,h.setValue(l))}}))}return f}disconnected(){this._$CK.disconnect(),this._$CX.pause()}reconnected(){this._$CK.reconnect(this),this._$CX.resume()}},Ke=_(de);function ce(t,e,s){Object.defineProperty(t,e,{configurable:!1,enumerable:!0,writable:!1,value:s})}var ue=class extends I{constructor(...t){super(...t),ce(this,"uuid",dt())}updated(...t){var e=t[0];super.updated(...t),this.observe&&e.forEach((s,i)=>{this.observe[i]&&this.observe[i](s,this[i])})}emit(t,e,s={}){this.dispatchEvent(new CustomEvent(t,{bubbles:!0,cancelable:!0,composed:!0,...s,detail:e}))}transition(t,e="stateService"){this[e]&&this[e].send(t)}is(t,e="state"){return t instanceof Array||(t=[t]),t.includes(this[e])}},Zt=class extends ue{static get prefix(){return ht()}};function Wt(){return Lt`

	:host{
		display: block;
	}

	h2{
		margin: 0;
		padding: 0;
	}

	`}var Xt=class extends Zt{static get styles(){return[Wt()]}};export{Jt as a,Ft as b,Lt as c,De as d,Xt as e};
/*! Bundled license information:

lit-html/lit-html.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)
*/
/*! Bundled license information:

lit-html/directive.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/unsafe-html.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)
*/
/*! Bundled license information:

@lit/reactive-element/css-tag.js:
  (**
   * @license
   * Copyright 2019 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

@lit/reactive-element/reactive-element.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-element/lit-element.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directive-helpers.js:
  (**
   * @license
   * Copyright 2020 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/async-directive.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/private-async-helpers.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/async-replace.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/async-append.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/cache.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/choose.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/class-map.js:
  (**
   * @license
   * Copyright 2018 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/guard.js:
  (**
   * @license
   * Copyright 2018 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/if-defined.js:
  (**
   * @license
   * Copyright 2018 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/join.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/keyed.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/live.js:
  (**
   * @license
   * Copyright 2020 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/map.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/range.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/ref.js:
  (**
   * @license
   * Copyright 2020 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/repeat.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/style-map.js:
  (**
   * @license
   * Copyright 2018 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/template-content.js:
  (**
   * @license
   * Copyright 2020 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/unsafe-svg.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/until.js:
  (**
   * @license
   * Copyright 2017 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)

lit-html/directives/when.js:
  (**
   * @license
   * Copyright 2021 Google LLC
   * SPDX-License-Identifier: BSD-3-Clause
   *)
*/
