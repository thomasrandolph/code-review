import { html } from "../../common/modules/@thomasrandolph/taproot/Component.js";

export function template( {
	initiate,
	logout,
	authenticated
} = {} ){
	return authenticated
		? signout( { logout } )
		: signin( { initiate } );
}

function signin( { initiate } = {} ){
	return html`

	<div unauthenticated>
		<button @click="${initiate}" login>
			🦊 Sign in with GitLab to start
		</button>
	</div>

	`;
}

function signout( { logout } = {} ){
	return html`

	<div authenticated>
		<button @click="${logout}" logout>Log Out</button>
	</div>

	`;
}
