import{a as s}from"./WOUKZX3Q.js";import{a as u,b as p,c as l,d as m}from"./OGHRZLRI.js";import"./2IE65OX3.js";import"./BX26ONIV.js";import{a as n,c,e as o}from"./5N6BXHGL.js";import"./6A2ISCT3.js";function d({initiate:t,logout:e,authenticated:i}={}){return i?C({logout:e}):T({initiate:t})}function T({initiate:t}={}){return n`

	<div unauthenticated>
		<button @click="${t}" login>
			🦊 Sign in with GitLab to start
		</button>
	</div>

	`}function C({logout:t}={}){return n`

	<div authenticated>
		<button @click="${t}" logout>Log Out</button>
	</div>

	`}function g(){return c`

	div{
		display: grid;
		gap: 0.5rem;
	}

	div[authenticated]{
		grid-template-columns: max-content 1fr;
		place-items: left;
	}

	div[unauthenticated]{
		grid-template-columns: 1fr 5fr 1fr;
		place-items: center;
	}

	button[login]{
		grid-column: 2;
	}

	`}var a="FAILURE",x="AUTHENTICATE",w="RETRY",A="CLEAR",R={waiting:{on:{[x]:"authing"}},authing:{on:{[a]:"failed"}},failed:{on:{[A]:"waiting",[w]:"authing"}}};function h({component:t,id:e,initial:i,propertyName:E="state",serviceName:b="stateService"}={}){var v=p({id:e||`authenticate-${t.uuid}`,initial:i||"waiting",states:R}),r=l(v);return m(t,r,E,b),r.start(),r}var f=class extends o{static get as(){return`${o.prefix}-authenticate`}static get styles(){return[...super.styles,g()]}static get properties(){return{}}constructor(...e){super(...e),this.subscriptions=[],h({component:this})}connectedCallback(...e){super.connectedCallback(...e),this.subscriptions.push(window.mcr.subscribeWith({"Core:Oauth:Fail":()=>{this.stateService.send(a)}})),window.mcr.publish("Core:Oauth:Upgrade")}disconnectedCallback(...e){this.subscriptions.forEach(i=>{i.unsubscribe()}),super.disconnectedCallback(...e)}render(){return d({authenticated:u(),initiate:()=>s(),logout:()=>window.mcr.publish("Core:Logout")})}};export{f as Authenticate};
