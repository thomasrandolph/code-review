// node_modules/@thomasrandolph/taproot/lib/Console.js
function log(...args) {
  console.log(...args);
}

// node_modules/@thomasrandolph/taproot/lib/requirements.js
var DEFAULT_NAMESPACE = "taproot";
var defaultTaproot = {
  "ready": false,
  "namespace": "TAPROOT",
  "errorLevel": 2
};
function namespaceExists() {
  return Boolean(window.NAMESPACE);
}
function taprootExists(namespace) {
  return Boolean(window[namespace]);
}
function getNamespace() {
  return namespaceExists() ? window.NAMESPACE : DEFAULT_NAMESPACE;
}
function getTaproot() {
  var namespace = getNamespace();
  return taprootExists(namespace) ? window[namespace] : defaultTaproot;
}
function namespaceMessage(message, ns) {
  return {
    ...message,
    "name": `${ns}|${message.name}`
  };
}

export {
  log,
  getNamespace,
  getTaproot,
  namespaceMessage
};
