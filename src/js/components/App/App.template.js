import { html, repeat } from "../../common/modules/@thomasrandolph/taproot/Component.js";

function mrUrl( { load = () => {}, disabled = false } ){
	return html`

	<label for="mrUrl">MR Url (only works on <code>gitlab.com</code> MRs you have access to)</label>
	<input
		id="mrUrl"
		type="text"
		placeholder="https://gitlab.com/gitlab-org/gitlab/-/merge_requests/101828"
		?disabled="${disabled}"
	/>
	<button @click="${load}" ?disabled="${disabled}">Load MR</button>

	`;
}

export function unauthenticated(){
	return html`<mcr-authenticate></mcr-authenticate>`;
}

export function loading(){
	return html`

	<mcr-authenticate></mcr-authenticate>
	<hr />
	${mrUrl( { load, "disabled": true } )}
	<hr />
	<p>Loading MR diff files...</p>

	`;
}

export function files( { files, observer, load } = {} ){
	return html`

	<mcr-authenticate></mcr-authenticate>
	<hr />
	${mrUrl( { load } )}
	<hr />
	${repeat(
		files,
		( file ) => file.uuid,
		( file ) => html`

		<mcr-diff-file
			.file="${file}"
			.observer="${observer}"
		>
		</mcr-diff-file>

		`
	)}

	`;
}

export function empty( { load } ){
	return html`

	<mcr-authenticate></mcr-authenticate>
	<hr />
	${mrUrl( { load } )}
	<hr />
	<h2>No files available to display</h2>

	`;
}

export function error(){
	return html`

	<mcr-authenticate></mcr-authenticate>
	<hr />
	${mrUrl( { load } )}
	<hr />
	<h2>No files available to display</h2>
	<p>There was a problem loading the MR.</p>
	`;
}
